## Bankenbot

Dieser Bankenbot wurde im Rahmen einer Masterthesis geschrieben und mit AWS Cloud Services realisiert. Der dahinterliegende Java Code f�r die Lambda Funktion unters�tzt dabei folgende Funktionen:

1. MultiIntent** Handling
2. Intent Switch
3. JSON Mapper von AWS mittels Jackson ersetzt
4. Kommunikation mit Lex �ber Input und Outputstream
2. Implemenation JSON Mapper mit Jackson

## Architektur des Chatbots
Das nachfolgende Bild zeigt die Grobarchitektur der Chatbot L�sung mit all Ihren Komponenten dar, welche auf Basis von Amazon Cloud Services entwickelt wurde. Die Pfeile stellen Kommunikationspfade dar uns sollen einen ersten Eindruck geben, welche Komponenten wie miteinander wie in Verbindung stehen. 

![alt text](http://marcelstalder.bplaced.net/Bankbot.jpg)

**Lex**
Lex ist ein Bestandteil der Amazon Cloud Service Infrastruktur. Mit diesem Service lassen sich textbasierte wie auch sprachgesteuerte Chatbots erstellen. Lex unterst�tzt das nat�rliche Sprachverst�ndnis (Natural Language Understanding, NLU) und die automatische Spracherkennung (Automatic Speech Recognition, ASR). Lex enth�lt auch eine Deep-Learning Funktion, welche keinerlei Kompetenzen seitens des Entwicklers erfordert. Der Entwickler selbst kann einen Gespr�chsfluss definieren und Lex verwaltet anschliessend die Dialoge und stimmt dynamisch die Antworten in dem Gespr�ch ab.

**Lambda **
AWS Lambda ist ein serverloser Datenverarbeitungsservice, der einen selbstprogrammierten Code, eine sogenannte Lambda Funktion, beim Eintreten von bestimmten Ereignissen startet. AWS Lambda erm�glicht es andere AWS Services anzubinden, welche im Rahmen dieses Prototyps ben�tigt werden. 
In dieser Arbeit wurde die Lambda Funktion �AWS_BANK_BOT_LAMBDA� erstellt. Diese Funktion beinhaltet die komplette Logik und Steuerung des Bankenchatbots. Zudem ist die Funktion im Stande mehrere Intents abwickeln zu k�nnen. Sie nimmt die JSON Requests, welche vom Benutzer �ber Lex an die Funktion gelangen entgegen und verarbeitet diese entsprechend und sendet den dazugeh�rigen Response mit den n�chsten Schritten an Amazon Lex zur�ck. Die Lambda Funktion wurde komplett in Java geschrieben, da diese KnowHow in der TKB am meisten verbreitet ist. Nachfolgend wird die grundlegende Architektur der Lambda Funktion aufgezeigt, wie deren Aufbau und Funktionsweise ist, um den Dialogfluss zwischen Benutzer und dem Chatbot steuern zu k�nnen. 

**DynamoDB **
Amazon DynamoDB ist ein vollst�ndig verwalteter propriet�rer NoSQL-Datenbankdienst, der Schl�sselwert- und Dokumentendatenstrukturen unterst�tzt und von Amazon als Teil des Amazon Web Services-Portfolios angeboten wird. DynamoDB verwendet die synchrone Replikation �ber mehrere Datencenter hinweg, um eine hohe Verf�gbarkeit zu gew�hrleisten. 
F�r den Bankbot wurden 2 Dynamo Tabellen angelegt. In der ersten Tabelle sind die Sicherheitsfragen gespeichert f�r den Identifizierungsprozess und in der anderen Tabelle die Benutzer mit den dazugeh�rigen Antworten zu jeder Sicherheitsfrage und die get�tigten Bankentransaktionen. 

**Mehr Informationen **
Mehr Informationen �ber den Bankenchatbot k�nnen unter marcel.stalder@gmail.com angefragt werden.