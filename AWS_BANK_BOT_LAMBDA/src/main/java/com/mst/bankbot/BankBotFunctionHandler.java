package com.mst.bankbot;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.IOUtils;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.mst.bankbot.db.DynamoDBService;
import com.mst.bankbot.intent.IntentService;
import com.mst.bankbot.lex.LexService;
import com.mst.bankbot.lex.model.LexRequest;
import com.mst.bankbot.lex.model.LexRespond;
import com.mst.bankbot.logging.LogService;
import com.mst.bankbot.question.QuestionService;
import com.mst.bankbot.validierung.ValidierungsService;

public class BankBotFunctionHandler implements RequestStreamHandler{


	private DynamoDBService db = DynamoDBService.getInstance();
	private LexService lex = LexService.getInstance();
	private QuestionService questionService = QuestionService.getInstance();
	private ValidierungsService validierungsService = ValidierungsService.getInstance();
	private IntentService intentService = IntentService.getInstance();
	

	@Override
    public void handleRequest(InputStream input, OutputStream output, Context context) throws JsonParseException, JsonMappingException, IOException {
    	LogService.setContext(context);
    	
    	LogService.log("---------------------------------------------------------------------------------------------");
    	LogService.log("---------- Start InputLex ----------");
    	@SuppressWarnings("deprecation")
		String jsonInput = IOUtils.toString(input);
    	LogService.log(jsonInput);
    	LogService.log("---------- End InputLex ----------");
    	
    	
    	
		LogService.log("---------- Start createLexRequestfromJson ----------");
		LexRequest lexRequest = lex.createLexRequestfromJson(jsonInput);
		lexRequest.printConsole();
		LogService.log("---------- End createLexRequestfromJson ----------");
		LexRespond lexResponse = null;
		
		
		switch(lexRequest.getCurrentIntent().getName()) {
		case "Reevaluate":
			if(lex.isAlreadyAuthenticted(lexRequest) || db.isAlreadyAuthenticated(lexRequest.getUserId())){
				lexResponse = intentService.handleReEvaluateIntent(lexRequest);
			} else {
				lexResponse = lex.switchToIdentifcationIntent();
			}
			break;
		case "PaymentReceive":
			if(lex.isAlreadyAuthenticted(lexRequest) || db.isAlreadyAuthenticated(lexRequest.getUserId())){
				lexResponse = intentService.handlePaymentReceiveIntent(lexRequest);
			} else {
				lexResponse = lex.switchToIdentifcationIntent();
			}
			break;
		case "PaymentSent":
			if(lex.isAlreadyAuthenticted(lexRequest) || db.isAlreadyAuthenticated(lexRequest.getUserId())){
				lexResponse = intentService.handlePaymentSentIntent(lexRequest);
			} else {
				lexResponse = lex.switchToIdentifcationIntent();
			}
			break;
		case "Identification":
			lexResponse = intentService.handleIdentificationIntent(lexRequest);
			break;
		}
		
		sendLexResponse(lexResponse,output);
    	
		
	}

    private void sendLexResponse(LexRespond lexResponse, OutputStream output) throws JsonGenerationException, JsonMappingException, IOException {
    	
    	/**************************************************
		 * Antwort an Lex senden
		 **************************************************/
		LogService.log("---------- Start LexResponse (modifiedJsonNodeAsString)--------------------------------------------------------------------------------------");
		ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false).configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		ObjectMapper objectMapperPrettyPrinting = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT).configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		
		JsonNode modifiedJsonNode = lex.prepareResponseBeforSending(lexResponse);
		//LogService.log(modifiedJsonNode.toString());
		LogService.log(objectMapperPrettyPrinting.writeValueAsString(modifiedJsonNode));
		objectMapper.writeValue(output, modifiedJsonNode);
		
			
		LogService.log("---------- End LexResponse (modifiedJsonNodeAsString)--------------------------------------------------------------------------------------");
		LogService.log("############################################################################################################################################");
    	
    }

}
