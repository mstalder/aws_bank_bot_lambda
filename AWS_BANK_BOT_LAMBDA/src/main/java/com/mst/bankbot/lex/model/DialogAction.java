package com.mst.bankbot.lex.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class DialogAction {

	//@JsonInclude(JsonInclude.Include.NON_NULL)
	
	private String type;
	@JsonInclude(Include.NON_NULL)
	private Slot slots;
	@JsonInclude(Include.NON_NULL)
	private String intentName;
	@JsonInclude(Include.NON_NULL)
	private String slotToElicit;
	@JsonInclude(Include.NON_NULL)
	private Message message;
	@JsonInclude(Include.NON_NULL)
	private ResponseCard responseCard;
	@JsonInclude(Include.NON_NULL)
	private String fulfillmentState;

	
	public DialogAction() {
		//default Contructor
	}
	
/**
 * Template für ElicitSlot
 * @param type 			ElicitSlot
 * @param slotToElicit	securityQuestion1
 * @param intentName	Identification
 * @param slots			slots
 */
	public DialogAction(String type, String slotToElicit, String intentName, Slot slots) {
		this.type = type;
		this.slotToElicit = slotToElicit;
		this.intentName = intentName;
		this.slots = slots;
	}

	
	/**
	 * Template für ConfirmIntent
	 * @param type
	 * @param message
	 * @param slots
	 */
	public DialogAction(String type, String intentToSwitch, Message message, Slot slots) {
		this.type = type;
		this.intentName = intentToSwitch;
		this.message = message;
		this.slots = slots;
	}	
	
	/**
	 * Template für ConfirmIntent
	 * @param type
	 * @param message
	 * @param slots
	 */
	public DialogAction(String type, Message message, Slot slots) {
		this.type = type;
		this.message = message;
		this.slots = slots;
	}

	/**
	 * Template für Close
	 * @param string
	 * @param fulfillmentState
	 * @param msg
	 */
	public DialogAction(String type, String fulfillmentState, Message msg) {
		this.type = type;
		this.fulfillmentState = fulfillmentState;
		this.message = msg;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Slot getSlots() {
		return slots;
	}

	public void setSlots(Slot slots) {
		this.slots = slots;
	}

	public String getIntentName() {
		return intentName;
	}

	public void setIntentName(String intentName) {
		this.intentName = intentName;
	}

	public ResponseCard getResponseCard() {
		return responseCard;
	}

	public void setResponseCard(ResponseCard responseCard) {
		this.responseCard = responseCard;
	}

	public String getSlotToElicit() {
		return slotToElicit;
	}

	public void setSlotToElicit(String slotToElicit) {
		this.slotToElicit = slotToElicit;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	public String getFulfillmentState() {
		return fulfillmentState;
	}

	public void setFulfillmentState(String fulfillmentState) {
		this.fulfillmentState = fulfillmentState;
	}

	
}
