package com.mst.bankbot.lex.model;

public class CurrentIntent {
	private String name;
	private Slot slots;
	private SlotDetails slotDetails;
	private String confirmationStatus;
	private String sourceLexNLUIntentInterpretation;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Slot getSlots() {
		return slots;
	}

	public void setSlots(Slot slots) {
		this.slots = slots;
	}

	public String getConfirmationStatus() {
		return confirmationStatus;
	}

	public void setConfirmationStatus(String confirmationStatus) {
		this.confirmationStatus = confirmationStatus;
	}

	public String getSourceLexNLUIntentInterpretation() {
		return sourceLexNLUIntentInterpretation;
	}

	public void setSourceLexNLUIntentInterpretation(String sourceLexNLUIntentInterpretation) {
		this.sourceLexNLUIntentInterpretation = sourceLexNLUIntentInterpretation;
	}

	public SlotDetails getSlotDetails() {
		return slotDetails;
	}

	public void setSlotDetails(SlotDetails slotDetails) {
		this.slotDetails = slotDetails;
	}
	
	

}
