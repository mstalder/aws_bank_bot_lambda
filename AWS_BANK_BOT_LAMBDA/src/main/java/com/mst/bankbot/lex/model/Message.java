package com.mst.bankbot.lex.model;

public class Message {
	private String contentType = "PlainText";
	private String content;

	public Message(String content) {
		this.content = content;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
