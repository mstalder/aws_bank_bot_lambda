package com.mst.bankbot.lex.model.identification;

import com.mst.bankbot.lex.model.Slot;

//@JsonTypeName("SlotsSbbAddNotification")
public class SlotsIdentification extends Slot {

	private String securityQuestionOne;
	private String securityQuestionTwo;
	private String securityQuestionThree;

	public SlotsIdentification() {
		// default Contructor
	}

	public String getSecurityQuestionOne() {
		return securityQuestionOne;
	}

	public void setSecurityQuestionOne(String securityQuestionOne) {
		this.securityQuestionOne = securityQuestionOne;
	}

	public String getSecurityQuestionTwo() {
		return securityQuestionTwo;
	}

	public void setSecurityQuestionTwo(String securityQuestionTwo) {
		this.securityQuestionTwo = securityQuestionTwo;
	}

	public String getSecurityQuestionThree() {
		return securityQuestionThree;
	}

	public void setSecurityQuestionThree(String securityQuestionThree) {
		this.securityQuestionThree = securityQuestionThree;
	}

}
