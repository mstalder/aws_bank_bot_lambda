package com.mst.bankbot.lex.model.payment;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mst.bankbot.lex.model.OriginalValue;
import com.mst.bankbot.lex.model.Slot;
import com.mst.bankbot.lex.model.SlotDetails;

public class SlotDetailPaymentReceive extends SlotDetails {

	private OriginalValue date;
	private OriginalValue amount;

	private SlotDetailPaymentReceive() {
		// default Constructor
	}

	public OriginalValue getDate() {
		return date;
	}

	public void setDate(OriginalValue date) {
		this.date = date;
	}

	public OriginalValue getAmount() {
		return amount;
	}

	public void setAmount(OriginalValue amount) {
		this.amount = amount;
	}

}
