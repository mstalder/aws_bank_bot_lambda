package com.mst.bankbot.lex.model.identification;

import com.mst.bankbot.lex.model.SessionAttributes;

public class SessionAttributesIdentification extends SessionAttributes {

	private int actualQuestion;
	private int securityQuestionOne;
	private int securityQuestionTwo;
	private int securityQuestionThree;
	private int securityQuestionAttemptOne = 0;
	private int securityQuestionAttemptTwo = 0;
	private int securityQuestionAttemptThree = 0;
	private String questionFailed;

	public SessionAttributesIdentification() {
		//default Constructor
	}

	public int getActualQuestion() {
		return actualQuestion;
	}

	public void setActualQuestion(int actualQuestion) {
		this.actualQuestion = actualQuestion;
	}

	public int getSecurityQuestionOne() {
		return securityQuestionOne;
	}

	public void setSecurityQuestionOne(int securityQuestionOne) {
		this.securityQuestionOne = securityQuestionOne;
	}

	public int getSecurityQuestionTwo() {
		return securityQuestionTwo;
	}

	public void setSecurityQuestionTwo(int securityQuestionTwo) {
		this.securityQuestionTwo = securityQuestionTwo;
	}

	public int getSecurityQuestionThree() {
		return securityQuestionThree;
	}

	public void setSecurityQuestionThree(int securityQuestionThree) {
		this.securityQuestionThree = securityQuestionThree;
	}

	public int getSecurityQuestionAttemptOne() {
		return securityQuestionAttemptOne;
	}

	public void setSecurityQuestionAttemptOne(int securityQuestionAttemptOne) {
		this.securityQuestionAttemptOne = securityQuestionAttemptOne;
	}

	public int getSecurityQuestionAttemptTwo() {
		return securityQuestionAttemptTwo;
	}

	public void setSecurityQuestionAttemptTwo(int securityQuestionAttemptTwo) {
		this.securityQuestionAttemptTwo = securityQuestionAttemptTwo;
	}

	public int getSecurityQuestionAttemptThree() {
		return securityQuestionAttemptThree;
	}

	public void setSecurityQuestionAttemptThree(int securityQuestionAttemptThree) {
		this.securityQuestionAttemptThree = securityQuestionAttemptThree;
	}

	public String getQuestionFailed() {
		return questionFailed;
	}

	public void setQuestionFailed(String questionFailed) {
		this.questionFailed = questionFailed;
	}
	

}
