package com.mst.bankbot.lex.model.payment;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mst.bankbot.lex.model.Slot;

public class SlotsPaymentReceive extends Slot {

	@JsonFormat 
	(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
	private Date date;
	private String amount;

	private SlotsPaymentReceive() {
		// default Constructor
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}


	

}
