package com.mst.bankbot.lex.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.mst.bankbot.lex.model.identification.SessionAttributesIdentification;
import com.mst.bankbot.lex.model.payment.SessionAttributesPaymentReceive;
import com.mst.bankbot.lex.model.payment.SessionAttributesPaymentSent;
import com.mst.bankbot.lex.model.reevaluate.SessionAttributesReEvaluate;;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY)
@JsonSubTypes({
  @JsonSubTypes.Type(value=SessionAttributesIdentification.class, name = "SessionAttributesIdentification"),
  @JsonSubTypes.Type(value=SessionAttributesPaymentReceive.class, name = "SessionAttributesPaymentReceive"),
  @JsonSubTypes.Type(value=SessionAttributesPaymentSent.class, name = "SessionAttributesPaymentSent"),
  @JsonSubTypes.Type(value=SessionAttributesReEvaluate.class, name = "SessionAttributesReEvaluate")
})
public abstract class SessionAttributes {
	
	private String identified;

	public String getIdentified() {
		return identified;
	}

	public void setIdentified(String identified) {
		this.identified = identified;
	}
	
	
}
