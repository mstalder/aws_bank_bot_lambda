package com.mst.bankbot.lex.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.mst.bankbot.lex.model.identification.SlotDetailIdentification;
import com.mst.bankbot.lex.model.payment.SlotDetailPaymentReceive;
import com.mst.bankbot.lex.model.payment.SlotDetailPaymentSent;
import com.mst.bankbot.lex.model.reevaluate.SlotDetailReEvaluate;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY)
@JsonSubTypes({
  @JsonSubTypes.Type(value=SlotDetailIdentification.class, name = "SlotDetailIdentification"),
  @JsonSubTypes.Type(value=SlotDetailPaymentReceive.class, name = "SlotDetailPaymentReceive"),
  @JsonSubTypes.Type(value=SlotDetailPaymentSent.class, name = "SlotDetailPaymentSent"),
  @JsonSubTypes.Type(value=SlotDetailReEvaluate.class, name = "SlotDetailReEvaluate")
})
public abstract class SlotDetails {

}
