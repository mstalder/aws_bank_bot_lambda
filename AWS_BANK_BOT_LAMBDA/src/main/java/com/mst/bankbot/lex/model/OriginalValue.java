package com.mst.bankbot.lex.model;

public class OriginalValue {
	private String originalValue;
	
	public OriginalValue() {
		//default Constructor
	}
	
	public String getOriginalValue() {
		return originalValue;
	}

	public void setOriginalValue(String originalValue) {
		this.originalValue = originalValue;
	}
	
}
