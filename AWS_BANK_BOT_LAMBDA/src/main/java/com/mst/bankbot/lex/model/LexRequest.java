package com.mst.bankbot.lex.model;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.mst.bankbot.logging.LogService;

public class LexRequest {

	private String messageVersion;
	private String invocationSource;
	private String userId;
	private SessionAttributes sessionAttributes;
	private RequestAttributes requestAttributes;
	private Bot bot;
	private String outputDialogMode;
	private CurrentIntent currentIntent;
	private String inputTranscript;
	
	public String getMessageVersion() {
		return messageVersion;
	}

	public void setMessageVersion(String messageVersion) {
		this.messageVersion = messageVersion;
	}

	public String getInvocationSource() {
		return invocationSource;
	}

	public void setInvocationSource(String invocationSource) {
		this.invocationSource = invocationSource;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}



	public SessionAttributes getSessionAttributes() {
		return sessionAttributes;
	}

	public void setSessionAttributes(SessionAttributes sessionAttributes) {
		this.sessionAttributes = sessionAttributes;
	}

	public RequestAttributes getRequestAttributes() {
		return requestAttributes;
	}

	public void setRequestAttributes(RequestAttributes requestAttributes) {
		this.requestAttributes = requestAttributes;
	}

	public Bot getBot() {
		return bot;
	}

	public void setBot(Bot bot) {
		this.bot = bot;
	}

	public String getOutputDialogMode() {
		return outputDialogMode;
	}

	public void setOutputDialogMode(String outputDialogMode) {
		this.outputDialogMode = outputDialogMode;
	}

	public CurrentIntent getCurrentIntent() {
		return currentIntent;
	}

	public void setCurrentIntent(CurrentIntent currentIntent) {
		this.currentIntent = currentIntent;
	}

	public String getInputTranscript() {
		return inputTranscript;
	}

	public void setInputTranscript(String inputTranscript) {
		this.inputTranscript = inputTranscript;
	}

	public void printConsole() throws IOException {
		
		ObjectMapper objectMapperPrettyPrinting = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT).configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		//LogService.log("---------- Start LexRequest--------------------------------------------------------------------------------------");
		LogService.log(objectMapperPrettyPrinting.writeValueAsString(this));
		//LogService.log("---------- End LexRequest--------------------------------------------------------------------------------------");
		
	}
}
