package com.mst.bankbot.lex.model;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ResponseCard {

	private String version;
	private String contentType;
	private ArrayList<GenericAttachment> genericAttachments;

	public ResponseCard(String version, String contentType, ArrayList<GenericAttachment> genericAttachments) {
		this.version = version;
		this.contentType = contentType;
		this.genericAttachments = genericAttachments;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public ArrayList<GenericAttachment> getGenericAttachments() {
		return genericAttachments;
	}

	public void setGenericAttachments(ArrayList<GenericAttachment> genericAttachments) {
		this.genericAttachments = genericAttachments;
	}

}
