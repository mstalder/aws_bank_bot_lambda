package com.mst.bankbot.lex.model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.mst.bankbot.lex.model.identification.SlotsIdentification;
import com.mst.bankbot.lex.model.payment.SlotsPaymentReceive;
import com.mst.bankbot.lex.model.payment.SlotsPaymentSent;
import com.mst.bankbot.lex.model.reevaluate.SlotsReEvaluate;
	
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY)
@JsonSubTypes({
  @JsonSubTypes.Type(value=SlotsIdentification.class, name = "SlotsIdentification"),
  @JsonSubTypes.Type(value=SlotsPaymentSent.class, name = "SlotsPaymentSent"),
  @JsonSubTypes.Type(value=SlotsPaymentReceive.class, name = "SlotsPaymentReceive"),
  @JsonSubTypes.Type(value=SlotsReEvaluate.class, name = "SlotsReEvaluate")
})
public abstract class Slot {
	//public String name;
}
