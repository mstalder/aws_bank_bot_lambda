package com.mst.bankbot.lex.model;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class GenericAttachment {
	private String title;
	private String subTitle;
	private String imageUrl;
	@JsonIgnore
	private String attachmentLinkUrl;
	private ArrayList<Button> buttons;

	public GenericAttachment(String title, String subTitle, String imageUrl, String attachmentLinkUrl,
			ArrayList<Button> buttons) {
		this.title = title;
		this.subTitle = subTitle;
		this.imageUrl = imageUrl;
		this.attachmentLinkUrl = attachmentLinkUrl;
		this.buttons = buttons;
		
	}
	
	
	public GenericAttachment(String title, String subTitle,ArrayList<Button> buttons) {
		this.title = title;
		this.subTitle = subTitle;
		this.buttons = buttons;
		
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSubTitle() {
		return subTitle;
	}

	public void setSubTitle(String subTitle) {
		this.subTitle = subTitle;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getAttachmentLinkUrl() {
		return attachmentLinkUrl;
	}

	public void setAttachmentLinkUrl(String attachmentLinkUrl) {
		this.attachmentLinkUrl = attachmentLinkUrl;
	}

	public ArrayList<Button> getButtons() {
		return buttons;
	}

	public void setButtons(ArrayList<Button> buttons) {
		this.buttons = buttons;
	}




}
