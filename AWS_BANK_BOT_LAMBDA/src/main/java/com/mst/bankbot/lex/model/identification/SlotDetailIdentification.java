package com.mst.bankbot.lex.model.identification;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mst.bankbot.lex.model.OriginalValue;
import com.mst.bankbot.lex.model.Slot;
import com.mst.bankbot.lex.model.SlotDetails;

public class SlotDetailIdentification extends SlotDetails {

	private OriginalValue securityQuestionOne;
	private OriginalValue securityQuestionTwo;
	private OriginalValue securityQuestionThree;

	private SlotDetailIdentification() {
		// default Constructor
	}

	public OriginalValue getSecurityQuestionOne() {
		return securityQuestionOne;
	}

	public void setSecurityQuestionOne(OriginalValue securityQuestionOne) {
		this.securityQuestionOne = securityQuestionOne;
	}

	public OriginalValue getSecurityQuestionTwo() {
		return securityQuestionTwo;
	}

	public void setSecurityQuestionTwo(OriginalValue securityQuestionTwo) {
		this.securityQuestionTwo = securityQuestionTwo;
	}

	public OriginalValue getSecurityQuestionThree() {
		return securityQuestionThree;
	}

	public void setSecurityQuestionThree(OriginalValue securityQuestionThree) {
		this.securityQuestionThree = securityQuestionThree;
	}

}
