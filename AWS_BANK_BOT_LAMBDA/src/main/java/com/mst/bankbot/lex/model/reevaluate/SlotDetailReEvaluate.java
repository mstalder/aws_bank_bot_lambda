package com.mst.bankbot.lex.model.reevaluate;

import com.mst.bankbot.lex.model.OriginalValue;
import com.mst.bankbot.lex.model.SlotDetails;

public class SlotDetailReEvaluate extends SlotDetails {

	private OriginalValue fake;
	private OriginalValue amount;

	private SlotDetailReEvaluate() {
		// default Constructor
	}

	public OriginalValue getFake() {
		return fake;
	}

	public void setFake(OriginalValue fake) {
		this.fake = fake;
	}

	public OriginalValue getAmount() {
		return amount;
	}

	public void setAmount(OriginalValue amount) {
		this.amount = amount;
	}



}
