package com.mst.bankbot.lex.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RequestAttributes {
	// private String facebook-page-id;

	@JsonProperty("x-amz-lex:facebook-page-id")
	private String facebookPageID; // EAAFxA9lvMV4BAL7da575Lix1zUg8scLhoRH94pSZBacdL2nIKVmqtH0AYmWQIZAUUyrHZBjqZCGvty7cgEZBINR8ZCjKiFRFWwkrwtwMqKcmyUbLNjMt9UQ7JGoEPz5Ps5CJOh1Ed8BOz7C1F2cCDZB7iAE0Eferm3pliJ58UZCskREvZBLO4LXiWZBHS12grED5EZD

	@JsonProperty("x-amz-lex:user-id") // 2231120193577993
	private String userId;

	@JsonProperty("x-amz-lex:channel-name")
	private String channelName; // SbbBot

	@JsonProperty("x-amz-lex:channel-type")
	private String channelType; // Facebook

	/**
	 * 
	 * @return Facebook Page ID (EAAFxA9lvMV4BAL7da575Lix1zUg8scLhoRH94pSZBacdL2nIKVmqtH0AYmWQIZAUUyrHZBjqZCGvty7cgEZBINR8ZCjKiFRFWwkrwtwMqKcmyUbLNjMt9UQ7JGoEPz5Ps5CJOh1Ed8BOz7C1F2cCDZB7iAE0Eferm3pliJ58UZCskREvZBLO4LXiWZBHS12grED5EZD)
	 */
	public String getFacebookPageID() {
		return facebookPageID;
	}

	public void setFacebookPageID(String facebookPageID) {
		this.facebookPageID = facebookPageID;
	}

	/**
	 * @return Facebook User ID (2231120193577993)
	 */
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return Facebook ChannelName (SBBBot)
	 */
	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	/**
	 * 
	 * @return ChannelTyp (Facebook)
	 */
	public String getChannelType() {
		return channelType;
	}

	public void setChannelType(String channelType) {
		this.channelType = channelType;
	}

	
}
