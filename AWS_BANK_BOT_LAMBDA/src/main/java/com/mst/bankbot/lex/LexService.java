package com.mst.bankbot.lex;

import java.io.IOException;
import java.util.ArrayList;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.mst.bankbot.lex.model.Button;
import com.mst.bankbot.lex.model.DialogAction;
import com.mst.bankbot.lex.model.GenericAttachment;
import com.mst.bankbot.lex.model.LexRequest;
import com.mst.bankbot.lex.model.LexRespond;
import com.mst.bankbot.lex.model.Message;
import com.mst.bankbot.lex.model.ResponseCard;
import com.mst.bankbot.lex.model.Slot;
import com.mst.bankbot.lex.model.identification.SessionAttributesIdentification;
import com.mst.bankbot.lex.model.identification.SlotsIdentification;
import com.mst.bankbot.lex.model.payment.SessionAttributesPaymentReceive;
import com.mst.bankbot.lex.model.payment.SessionAttributesPaymentSent;
import com.mst.bankbot.lex.model.payment.SlotsPaymentReceive;
import com.mst.bankbot.lex.model.payment.SlotsPaymentSent;
import com.mst.bankbot.logging.LogService;

public class LexService {


	private static LexService instance;
	
	public static LexService getInstance() {
		if(instance == null) {
			instance = new LexService();
		}
		return instance;
	}

	
	
	public LexRequest createLexRequestfromJson(String jsonStr) throws JsonParseException, JsonMappingException, IOException { 
		// Parsing Input Request von Lex inkl. Informationen von Facebook Messenger!
		/** Object Mapper **/
		ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false).configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		ObjectMapper objectMapperPrettyPrinting = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT).configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);


		//Add Property für Slot
		JsonNode rootNode = objectMapperPrettyPrinting.readTree(jsonStr);

		//Welches Intent? Wirde benötigt für die spätere Serialisierung von Slots und SessionAttributes
		String intentName = rootNode.at("/currentIntent/name").asText();
		LogService.log("UsedIntent: " + intentName);

		//Vorbereitung für Derealisierung
		JsonNode locatedNode;
		switch(intentName) {
		case "PaymentReceive":
			//Slots
			locatedNode = rootNode.path("currentIntent").path("slots");
			((ObjectNode) locatedNode).put("@type", "SlotsPaymentReceive");
			
			//SlotDetails
			locatedNode = rootNode.path("currentIntent").path("slotDetails");
			((ObjectNode) locatedNode).put("@type", "SlotDetailPaymentReceive");
			
			
			//SessionAttributes
			if(!rootNode.hasNonNull("sessionAttributes")) {
				((ObjectNode) rootNode).put("sessionAttributes", objectMapper.valueToTree(new SessionAttributesPaymentReceive()));
			}			
			locatedNode = rootNode.path("sessionAttributes");
			((ObjectNode) locatedNode).put("@type", "SessionAttributesPaymentReceive");
			
			break;
		case "PaymentSent":
			//Slots
			locatedNode = rootNode.path("currentIntent").path("slots");
			((ObjectNode) locatedNode).put("@type", "SlotsPaymentSent");

			//SlotDetails
			locatedNode = rootNode.path("currentIntent").path("slotDetails");
			((ObjectNode) locatedNode).put("@type", "SlotDetailPaymentSent");
			
			
			//SessionAttributes
			if(!rootNode.hasNonNull("sessionAttributes")) {
				((ObjectNode) rootNode).put("sessionAttributes", objectMapper.valueToTree(new SessionAttributesPaymentSent()));
			}			
			locatedNode = rootNode.path("sessionAttributes");
			((ObjectNode) locatedNode).put("@type", "SessionAttributesPaymentSent");
			
			break;	
		case "Identification":
			//Slots
			locatedNode = rootNode.path("currentIntent").path("slots");
			((ObjectNode) locatedNode).put("@type", "SlotsIdentification");
			
			//SlotDetails
			locatedNode = rootNode.path("currentIntent").path("slotDetails");
			((ObjectNode) locatedNode).put("@type", "SlotDetailIdentification");
			
			//SessionAttributes
			if(!rootNode.hasNonNull("sessionAttributes")) {
				((ObjectNode) rootNode).put("sessionAttributes", objectMapper.valueToTree(new SessionAttributesIdentification()));
			}		
			locatedNode = rootNode.path("sessionAttributes");
			((ObjectNode) locatedNode).put("@type", "SessionAttributesIdentification");
			break;
		case "Reevaluate":
			//Slots
			locatedNode = rootNode.path("currentIntent").path("slots");
			((ObjectNode) locatedNode).put("@type", "SlotsReEvaluate");
			
			//SlotDetails
			locatedNode = rootNode.path("currentIntent").path("slotDetails");
			((ObjectNode) locatedNode).put("@type", "SlotDetailReEvaluate");
			
			//SessionAttributes
			if(!rootNode.hasNonNull("sessionAttributes")) {
				((ObjectNode) rootNode).put("sessionAttributes", objectMapper.valueToTree(new SessionAttributesIdentification()));
			}		
			locatedNode = rootNode.path("sessionAttributes");
			((ObjectNode) locatedNode).put("@type", "SessionAttributesReEvaluate");
			break;
		}
		
		LexRequest lexRequest = objectMapper.readValue(objectMapper.writeValueAsString(rootNode), LexRequest.class);

		return lexRequest;
	}



	public LexRespond switchToIdentifcationIntent() {
		
		Slot slots = new SlotsIdentification();
		Message message = new Message("Du hast aktuell keine Berechtigung Account Abfragen zu tätigen");
		DialogAction dialogAction = new DialogAction("ConfirmIntent", "Identification", message, slots);
		dialogAction.setResponseCard(createResponseCardForSwitchIntent());
		
		LexRespond lexRespond = new LexRespond();
		lexRespond.setDialogAction(dialogAction);
		
		return lexRespond;
	}
	
	public LexRespond createChooseIntent() {
		
		DialogAction dialogAction = new DialogAction();
		dialogAction.setType("ElicitIntent");
		dialogAction.setMessage(new Message("Was möchtest Du wissen?"));
		dialogAction.setResponseCard(createResponseCardChoooseIntent());
		
		LexRespond lexRespond = new LexRespond();
		lexRespond.setDialogAction(dialogAction);
		
		return lexRespond;
	}



	/**
	 * Modifikationen aufgrund Jackson Serialisierer
	 * - sessionAttribut
	 * - Slots
	 * @param lexResponse
	 * @return JsonNode (Rueckgabe eines Strings funktioniert nicht!) 
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public JsonNode prepareResponseBeforSending(LexRespond lexResponse) throws JsonProcessingException, IOException {

		ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false).configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		
		//Lösche Deserialsierungs Attribute für Jackson
		JsonNode rootNode = objectMapper.readTree(objectMapper.writeValueAsString(lexResponse));
		if(rootNode.toString().contains("ElicitSlot") || rootNode.toString().contains("ConfirmIntent") || rootNode.toString().contains("Delegate") ){

			//Handle Slots
			if(rootNode.path("dialogAction").has("slots")) {
				JsonNode slotNode = rootNode.path("dialogAction").path("slots");
				((ObjectNode) slotNode).remove("@type");
				//!slotNode.isNull()
			}
			

			//Handle SessionAttributes
			if(rootNode.has("sessionAttributes")) {
				JsonNode sessionAttributesNode = rootNode.path("sessionAttributes");
				((ObjectNode) sessionAttributesNode).remove("@type");
				//sessionAttributesNode.isNull()
			}
				
		}
		
		//return objectMapper.writeValueAsString(rootNode);
		return rootNode;
		
	}


	
	/**
	 * 
	 * @param slotToElicit  Next Slot which has to be asked
	 * @param lexRequest    Lex Request
	 */
	public LexRespond createElicitSlot(String slotToElicit,LexRequest lexRequest, ResponseCard responseCard){
		LexRespond lexRespond = new LexRespond();
		
		Slot slots = null;
		switch(lexRequest.getCurrentIntent().getName()) {
			case("PaymentReceive"): {
				slots = (SlotsPaymentReceive) lexRequest.getCurrentIntent().getSlots();
				break;
			}
			case("PaymentSent"): {
				slots = (SlotsPaymentSent) lexRequest.getCurrentIntent().getSlots();
				break;
			}
			case("Identification"):{
				slots = (SlotsIdentification) lexRequest.getCurrentIntent().getSlots();
				break;
			}
		}
		
		DialogAction dialogAction = new DialogAction("ElicitSlot", slotToElicit, lexRequest.getCurrentIntent().getName(), slots);
		lexRespond.setDialogAction(dialogAction);
		
		if(responseCard != null) {
			lexRespond.getDialogAction().setResponseCard(responseCard);
		}
		
		return lexRespond;
	}



	public boolean isAlreadyAuthenticted(LexRequest lexRequest) {
		
		//Check SessionAttributes
		if(lexRequest.getSessionAttributes().getIdentified() != null && lexRequest.getSessionAttributes().getIdentified().equalsIgnoreCase("true")) {
			return true;
		} else {
			return false;
		}
	}
	
	

	
	/**
	 * 
	 * @param sbbResponse
	 * @return
	 */
	public ResponseCard createResponseCardForSwitchIntent() {
		//Create Buttons
		ArrayList<Button> buttons = new ArrayList<Button>();
		Button yes = new Button("Ja gerne!", "yes");
		Button no = new Button("Nein Danke", "no");
		
		buttons.add(yes);
		buttons.add(no);
				
		//Create ResponseCard
		ArrayList<GenericAttachment> genericAttachments = new ArrayList<GenericAttachment>();
		//GenericAttachment genAt1 = new GenericAttachment("Verbindungen","Von Zürich HB nach Kreuzlingen am Montag 25.11.2019","https://www.sbb.ch/content/dam/internet/zug/EC250_SBB_HR_2_63189.jpg/_jcr_content/renditions/cq5dam.web.1280.1280.jpeg","www.sbb.ch",buttons);
		GenericAttachment genAt1 = new GenericAttachment("Wir müssen Sie zuerst identifizieren","Später werden Sie über ihr Facebook automatisch identifziert, falls erwünscht","https://www.fbi.gov/image-repository/generation-identification.jpg/@@images/image/large","www.tkb.ch",buttons);
		genericAttachments.add(genAt1);
		ResponseCard responseCard = new ResponseCard("1","application/vnd.amazonaws.card.generic",genericAttachments);
			
		return responseCard;
	}


	/**
	 * 
	 * @param sbbResponse
	 * @return
	 */
	public ResponseCard createResponseCardChoooseIntent() {
		//Create Buttons
		ArrayList<Button> buttons = new ArrayList<Button>();
		Button paymentReceived = new Button("Erhaltene Bezahlungen", "Was fuer Zahlungen sind eingegangen");
		Button payments= new Button("Ausgeloeste Bezahlungen", "Was fuer Zahlungen sind ausgeloest worden");
		
		buttons.add(paymentReceived);
		buttons.add(payments);
				
		//Create ResponseCard
		ArrayList<GenericAttachment> genericAttachments = new ArrayList<GenericAttachment>();
		GenericAttachment genAt1 = new GenericAttachment("Was möchten Sie tun","Verfügbare Optionen",buttons);
		genericAttachments.add(genAt1);
		ResponseCard responseCard = new ResponseCard("1","application/vnd.amazonaws.card.generic",genericAttachments);
			
		return responseCard;
	}
	
	/**
	 * 
	 * @param sbbResponse
	 * @return
	 */
	public ResponseCard createResponseCardForIdentificationSaveYesOrNot() {
		//Create Buttons
		ArrayList<Button> buttons = new ArrayList<Button>();
		Button yes = new Button("Ja", "yes");
		Button no = new Button("Nein", "no");
		
		buttons.add(yes);
		buttons.add(no);
				
		//Create ResponseCard
		ArrayList<GenericAttachment> genericAttachments = new ArrayList<GenericAttachment>();
		//GenericAttachment genAt1 = new GenericAttachment("Verbindungen","Von Zürich HB nach Kreuzlingen am Montag 25.11.2019","https://www.sbb.ch/content/dam/internet/zug/EC250_SBB_HR_2_63189.jpg/_jcr_content/renditions/cq5dam.web.1280.1280.jpeg","www.sbb.ch",buttons);
		GenericAttachment genAt1 = new GenericAttachment("Identifizierung abgreschlossen","Möchtest du deine Identifizierung speichern für weitere Anfragen?","https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTj4McYbQ2mR65zhFh8Gp0Oy0lHXQnRgCa36AjRHznXOrtVJ6Y1ew","www.tkb.ch",buttons);
		genericAttachments.add(genAt1);
		ResponseCard responseCard = new ResponseCard("1","application/vnd.amazonaws.card.generic",genericAttachments);
			
		return responseCard;
	}

	


	/**
	 * Beendet die Kommunikation mit dem Kunden.
	 * @param message
	 * @param fullfillmentState  Failed / Fulfilled
	 * @return
	 */
	public LexRespond createCloseResponseIdentification(String message, String fullfillmentState) {

		Message msg = new Message(message);
		DialogAction dialogAction = new DialogAction("Close", "Failed", msg);
		LexRespond lexRespond = new LexRespond();
		lexRespond.setDialogAction(dialogAction);
		
		return lexRespond;
	}



	public LexRespond createConfirmIntentIdentifiedSaveYesOrNo(LexRequest lexRequest) {
		
		
		
		SlotsIdentification slots = ((SlotsIdentification) lexRequest.getCurrentIntent().getSlots());
		Message message = new Message("Identifizierung abgeschlossen");
		
		DialogAction dialogAction = new DialogAction("ConfirmIntent", message, slots);
		dialogAction.setIntentName("Identification");
		
		dialogAction.setResponseCard(createResponseCardForIdentificationSaveYesOrNot());
		
		LexRespond lexRespond = new LexRespond();
		lexRespond.setDialogAction(dialogAction);
		
		return lexRespond;
	}
	

	
	
}
