package com.mst.bankbot.lex.model;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.mst.bankbot.logging.LogService;

public class LexRespond {
	
	@JsonInclude(Include.NON_NULL)
	private SessionAttributes sessionAttributes;
	private DialogAction dialogAction;

	public LexRespond(DialogAction dialogAction) {
		this.dialogAction = dialogAction;
	}

	public LexRespond() {
	}

	public DialogAction getDialogAction() {
		return dialogAction;
	}

	public void setDialogAction(DialogAction dialogAction) {
		this.dialogAction = dialogAction;
	}
	
	
	public SessionAttributes getSessionAttributes() {
		return sessionAttributes;
	}

	public void setSessionAttributes(SessionAttributes sessionAttributes) {
		this.sessionAttributes = sessionAttributes;
	}

	/**
	 * Print lexRespond in Json Format
	 * @throws IOException
	 */
	public void printConsole() throws IOException {
		
		ObjectMapper objectMapperPrettyPrinting = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT).configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		LogService.log(objectMapperPrettyPrinting.writeValueAsString(this));

		
	}
	
	public void getJson() throws IOException {
		
		ObjectMapper objectMapperPrettyPrinting = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT).configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
		LogService.log("---------- Start LexRespond--------------------------------------------------------------------------------------");
		LogService.log(objectMapperPrettyPrinting.writeValueAsString(this));
		LogService.log("---------- End LexRespond--------------------------------------------------------------------------------------");
		
	}

	
	
	
}
