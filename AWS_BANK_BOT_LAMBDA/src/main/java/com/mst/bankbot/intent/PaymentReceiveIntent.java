package com.mst.bankbot.intent;

import java.util.ArrayList;
import java.util.StringTokenizer;

import org.apache.commons.lang3.time.DateFormatUtils;

import com.mst.bankbot.db.DynamoDBService;
import com.mst.bankbot.db.model.PaymentReceiveItem;
import com.mst.bankbot.lex.model.DialogAction;
import com.mst.bankbot.lex.model.LexRequest;
import com.mst.bankbot.lex.model.LexRespond;
import com.mst.bankbot.lex.model.Message;
import com.mst.bankbot.lex.model.payment.SlotDetailPaymentReceive;
import com.mst.bankbot.lex.model.payment.SlotsPaymentReceive;
import com.mst.bankbot.logging.LogService;

public class PaymentReceiveIntent {
	
	private DynamoDBService db = DynamoDBService.getInstance();

	public LexRespond handleIt(LexRequest lexRequest) {
		
		LexRespond lexResponse = new LexRespond();
		
		//Übernahme Session Attribute
		lexResponse.setSessionAttributes(lexRequest.getSessionAttributes());
		
		
		SlotsPaymentReceive slots = ((SlotsPaymentReceive) lexRequest.getCurrentIntent().getSlots());
		if(slots.getAmount() != null && slots.getDate() != null ) {
			

			//DatumFix für LexSlot und Format!
			String dateModified = ((SlotDetailPaymentReceive)lexRequest.getCurrentIntent().getSlotDetails()).getDate().getOriginalValue();
			dateModified = formatDate(dateModified);
			
			//Amount Fix
			String amountModified = fixAmount(slots.getAmount());
			
			LogService.log("Look for received payments: " + dateModified + " " + amountModified);
			
			ArrayList<PaymentReceiveItem> paymentReceiveList = db.getAllPaymentReceivedFromPerson(lexRequest.getUserId(),amountModified,dateModified);
			
			//Sent back to client mit Close Event:
			// Message vorbereiten
			String msg = "";
			if(paymentReceiveList.size() > 1 ) {
				msg = "Du hast " + paymentReceiveList.size() + " Beträge überwiesen bekommen \n\n";
			} else if (paymentReceiveList.size() == 1) {
				msg = "Du hast den folgenden Betrag überwiesen bekommen \n\n";
			}
			
			int i = 1;
			for (PaymentReceiveItem p : paymentReceiveList) {
				msg = msg + i + ".) " + p.getAmount() + "Fr am " + p.getReceiveDate() + " von " + p.getFrom() + "\n\n";
			}
			
			if(msg.equals("")) {
				msg = "Es konnte kein Zahlungseingang von "+ slots.getAmount() + " gefunden werden"; 
			}
			
			DialogAction dialogAction = new DialogAction();
			dialogAction.setType("Close");
			dialogAction.setFulfillmentState("Fulfilled");
			dialogAction.setMessage(new Message(msg));
			
			lexResponse.setDialogAction(dialogAction);
			
		} else {
			//Delegate to Lex
			LogService.log("Create Delegate Request =" + slots.getAmount() + " date=" + slots.getDate());
			DialogAction dialogAction = new DialogAction();
			dialogAction.setType("Delegate");
			dialogAction.setSlots(slots);
			
			lexResponse.setDialogAction(dialogAction);
		}
				
		return lexResponse;
	}
	
	public String fixAmount(String inputAmount) {
		inputAmount = inputAmount.replaceAll("[^\\d.]", "");
		inputAmount = inputAmount.replaceAll("[^\\d]", "");    //2x ausfhuehren, da sonst noch ein "." vorhanden sein kann
		return inputAmount;
	}
	
	public String formatDate(String inputDate) {
		
		//Lösche alle Zeichen
		inputDate = inputDate.replaceAll("[^\\d.]", "");
		System.out.println(inputDate);
		
		//Hole mit Tag,Monat und Jahr
		StringTokenizer stok = new StringTokenizer(inputDate, ".");
		ArrayList<String> dateArray = new ArrayList<String>();        
		while (stok.hasMoreTokens()){
			dateArray.add(stok.nextToken());
		}
		String tag = dateArray.get(0);
		String monat = dateArray.get(1);
		String jahr = dateArray.get(2);
		
		
		//Mache Korrekturen
		if(tag.length() == 1) {
			tag = "0" + tag;
		}
		if(monat.length() == 1) {
			monat = "0" + monat;
		}
		if(jahr.length() == 2) {
			jahr = "20" + jahr;
		}
		
		return (tag + "." + monat + "." + jahr);
	}

}
