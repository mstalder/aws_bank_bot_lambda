package com.mst.bankbot.intent;

import com.mst.bankbot.db.DynamoDBService;
import com.mst.bankbot.lex.LexService;
import com.mst.bankbot.lex.model.LexRequest;
import com.mst.bankbot.lex.model.LexRespond;

public class ReEvaluateIntent {
	
	private DynamoDBService db = DynamoDBService.getInstance();
	private LexService lexService = LexService.getInstance();

	public LexRespond handleIt(LexRequest lexRequest) {
				
		return lexService.createChooseIntent();
	}


}
