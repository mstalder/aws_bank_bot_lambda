package com.mst.bankbot.intent;

import java.util.ArrayList;
import java.util.StringTokenizer;

import com.mst.bankbot.db.DynamoDBService;
import com.mst.bankbot.db.model.PaymentSentItem;
import com.mst.bankbot.lex.model.DialogAction;
import com.mst.bankbot.lex.model.LexRequest;
import com.mst.bankbot.lex.model.LexRespond;
import com.mst.bankbot.lex.model.Message;
import com.mst.bankbot.lex.model.payment.SlotDetailPaymentSent;
import com.mst.bankbot.lex.model.payment.SlotsPaymentSent;
import com.mst.bankbot.logging.LogService;

public class PaymentSentIntent {

	private DynamoDBService db = DynamoDBService.getInstance();
	
	public LexRespond handleIt(LexRequest lexRequest) {
		LexRespond lexResponse = new LexRespond();
		
		//Übernahme Session Attribute
		lexResponse.setSessionAttributes(lexRequest.getSessionAttributes());
		
		
		SlotsPaymentSent slots = ((SlotsPaymentSent) lexRequest.getCurrentIntent().getSlots());
		if(slots.getAmount() != null && slots.getDate() != null ) {
			

			//DatumFix für LexSlot und Format!
			String dateModified = ((SlotDetailPaymentSent)lexRequest.getCurrentIntent().getSlotDetails()).getDate().getOriginalValue();
			dateModified = formatDate(dateModified);
			LogService.log("Look for sent payments: " + dateModified + " " + slots.getAmount());
			
			//Amount Fix
			String amountModified = fixAmount(slots.getAmount());
			
			//Get all PaymentsSent
			ArrayList<PaymentSentItem> paymentSentList = db.getAllPaymentSentFromPerson(lexRequest.getUserId(),amountModified,dateModified);
			
			//Sent back to client mit Close Event:
			String msg = "";

			if(paymentSentList.size() > 1 ) {
				msg = "Du hast " + paymentSentList.size() + " Zahlungen bezahlt\n\n";
			} else {
				msg = "Du hast den folgenden Betrag bezahlt: \n\n";
			}
			
			int i = 1;
			for (PaymentSentItem p : paymentSentList) {
				msg = msg + i + ".) " + p.getAmount() + "Fr am " + p.getSentDate() + " an " + p.getTo()+ "\n\n";
			}
			
			if(msg.equals("")) {
				msg = "Es wurde keine Zahlungen von "+ slots.getAmount() + " ausgelöst im Zeitraum " + dateModified; 
			}
			
			DialogAction dialogAction = new DialogAction();
			dialogAction.setType("Close");
			dialogAction.setFulfillmentState("Fulfilled");
			dialogAction.setMessage(new Message(msg));
			
			lexResponse.setDialogAction(dialogAction);
			
		} else {
			//Delegate to Lex
			LogService.log("Create Delegate Request =" + slots.getAmount() + " date=" + slots.getDate());
			DialogAction dialogAction = new DialogAction();
			dialogAction.setType("Delegate");
			dialogAction.setSlots(slots);
			
			lexResponse.setDialogAction(dialogAction);
		}
				
		return lexResponse;
	}
	
	public String fixAmount(String inputAmount) {
		inputAmount = inputAmount.replaceAll("[^\\d.]", "");
		inputAmount = inputAmount.replaceAll("[^\\d]", "");    //2x ausfhuehren, da sonst noch ein "." vorhanden sein kann
		return inputAmount;
	}
	
	public String formatDate(String inputDate) {
		
		//Lösche alle Zeichen
		inputDate = inputDate.replaceAll("[^\\d.]", "");
		System.out.println(inputDate);
		
		//Hole mit Tag,Monat und Jahr
		StringTokenizer stok = new StringTokenizer(inputDate, ".");
		ArrayList<String> dateArray = new ArrayList<String>();        
		while (stok.hasMoreTokens()){
			dateArray.add(stok.nextToken());
		}
		String tag = dateArray.get(0);
		String monat = dateArray.get(1);
		String jahr = dateArray.get(2);
		
		
		//Mache Korrekturen
		if(tag.length() == 1) {
			tag = "0" + tag;
		}
		if(monat.length() == 1) {
			monat = "0" + monat;
		}
		if(jahr.length() == 2) {
			jahr = "20" + jahr;
		}
		
		return (tag + "." + monat + "." + jahr);
	}
	

}
