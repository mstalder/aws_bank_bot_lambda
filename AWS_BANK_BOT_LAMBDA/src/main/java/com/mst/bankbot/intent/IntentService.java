package com.mst.bankbot.intent;

import com.mst.bankbot.lex.model.LexRequest;
import com.mst.bankbot.lex.model.LexRespond;

public class IntentService {

	
	private static IntentService instance;
	
	public static IntentService getInstance() {
		if(instance == null) {
			instance = new IntentService();
		}
		return instance;
	}

	public LexRespond handlePaymentReceiveIntent(LexRequest lexRequest) {
		return new PaymentReceiveIntent().handleIt(lexRequest);
	}

	public LexRespond handlePaymentSentIntent(LexRequest lexRequest) {
		return new PaymentSentIntent().handleIt(lexRequest);
	}
	
	public LexRespond handleReEvaluateIntent(LexRequest lexRequest) {
		return new ReEvaluateIntent().handleIt(lexRequest);
	}
	
	public LexRespond handleIdentificationIntent(LexRequest lexRequest) {
		return new IdentificationIntent().handleIt(lexRequest); 
	}
	
	
	
	
}
