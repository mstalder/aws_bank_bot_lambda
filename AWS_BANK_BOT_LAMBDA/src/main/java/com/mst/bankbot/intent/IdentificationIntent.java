package com.mst.bankbot.intent;

import com.mst.bankbot.db.DynamoDBService;
import com.mst.bankbot.lex.LexService;
import com.mst.bankbot.lex.model.LexRequest;
import com.mst.bankbot.lex.model.LexRespond;
import com.mst.bankbot.lex.model.Message;
import com.mst.bankbot.lex.model.identification.SessionAttributesIdentification;
import com.mst.bankbot.lex.model.identification.SlotsIdentification;
import com.mst.bankbot.logging.LogService;
import com.mst.bankbot.question.QuestionService;
import com.mst.bankbot.question.model.QuestionItem;
import com.mst.bankbot.validierung.ValidierungsService;

public class IdentificationIntent {

	private DynamoDBService db = DynamoDBService.getInstance();
	private LexService lex = LexService.getInstance();
	private QuestionService questionService = QuestionService.getInstance();
	private ValidierungsService validierungsService = ValidierungsService.getInstance();

	public LexRespond handleIt(LexRequest lexRequest) {

		LexRespond lexResponse = new LexRespond();

		// Prüfe DialogCodehook: confirmed/None -> Weiter mit Identification / Denied ->
		// Send Close
		if (lexRequest.getCurrentIntent().getConfirmationStatus().equals("Denied")) {
			// Send Close Intent
			lexResponse = lex.createCloseResponseIdentification(
					"Sie können die Registrierung jederzeit nacholen. Ich wünsche Ihnen noch einen schönen Tag.",
					"Failed");
		} else {
			// DialogCodeHook = confirmed/None
			SlotsIdentification requestSlots = (SlotsIdentification) lexRequest.getCurrentIntent().getSlots();
			if (requestSlots.getSecurityQuestionOne() == null) {
				// 1. Create lexResponse
				lexResponse = lex.createElicitSlot("securityQuestionOne", lexRequest, null);

				// 2. Setze SessionAttributes
				lexResponse.setSessionAttributes(new SessionAttributesIdentification());
				lexResponse.getSessionAttributes().setIdentified("false");

				// 3. Get and set SecurityQuestion
				QuestionItem question = questionService.getQuestionRandom();
				lexResponse.getDialogAction().setMessage(new Message(question.getQuestion()));

				// 4. Aktualisiere SessionAttribute (werden nicht gesetzt)
				((SessionAttributesIdentification) lexResponse.getSessionAttributes())
						.setActualQuestion(question.getId());
				((SessionAttributesIdentification) lexResponse.getSessionAttributes())
						.setSecurityQuestionOne(question.getId());

			} else if (requestSlots.getSecurityQuestionTwo() == null) {
				// 1. Create lexResponse
				lexResponse = lex.createElicitSlot("securityQuestionTwo", lexRequest, null);

				// 2. Übernahme Session Attribute
				lexResponse.setSessionAttributes(lexRequest.getSessionAttributes());

				// 3. Validierung Question1 (userid, input, frag)
				Boolean val = validierungsService.validate(lexRequest.getUserId(), lexRequest.getInputTranscript(),
						((SessionAttributesIdentification) lexRequest.getSessionAttributes()).getSecurityQuestionOne());
				LogService.log("Validierung Question1: " + val + "   (Parameter: InputTranscript="
						+ lexRequest.getInputTranscript() + ", securityQuestionOne="
						+ ((SessionAttributesIdentification) lexRequest.getSessionAttributes())
								.getSecurityQuestionOne());
				if (!val) {
					// Wieviel mal falsch? Jeder hat 2 Versuche. Danach Question Wechsel. Wieder
					// falsch -> Abbruch.
					// Wurde die SecurityQuestion 1 schonmal falsch beantwortet?
					int securityQuestionOneAttempt = ((SessionAttributesIdentification) lexResponse
							.getSessionAttributes()).getSecurityQuestionAttemptOne();
					if (securityQuestionOneAttempt >= 1) {
						// Send Close Message
						lexResponse = lex.createCloseResponseIdentification(
								"Leider wurden die gestellten Secrityfragen nicht korrekt beantwortet. Melden Sie sich unter 0848 111 444. Sie können die Registrierung auch jederzeit nacholen. Ich wünsche Ihnen noch einen schönen Tag. ",
								"Failed");

					} else {
						// 1.) Attempt auf 1 setzen
						((SessionAttributesIdentification) lexResponse.getSessionAttributes())
								.setSecurityQuestionAttemptOne(1);

						// 2.) Hole nächste Securityfrage
						QuestionItem q = questionService.getNextQuestion(lexResponse);
						lexResponse.getDialogAction()
								.setMessage(new Message(
										"Ihre Antwort war leider nicht richtig. Bitte versuchen sie die nächste Frage.\n\n "
												+ q.getQuestion()));

						// 3.) Aktualisiere SessionAttribute (securityQuestion1 und actualQuestion)
						((SessionAttributesIdentification) lexResponse.getSessionAttributes())
								.setActualQuestion(q.getId());
						((SessionAttributesIdentification) lexResponse.getSessionAttributes())
								.setSecurityQuestionOne(q.getId());

						// 4.) Überschreibe SlotoElicit und setze SQ1 wieder auf null
						lexResponse.getDialogAction().setSlotToElicit("securityQuestionOne");
						((SlotsIdentification) lexResponse.getDialogAction().getSlots()).setSecurityQuestionOne(null);

						// 5.) Sende lexResponse zurück
					}
				} else {
					// falls die Frage korrekt ist!
					// 1.) Hole nächste Securityfrage
					QuestionItem q = questionService.getNextQuestion(lexResponse);
					lexResponse.getDialogAction().setMessage(new Message(q.getQuestion()));

					// 2.) Aktualisiere SessionAttribute (securityQuestion1 und actualQuestion)
					((SessionAttributesIdentification) lexResponse.getSessionAttributes()).setActualQuestion(q.getId());
					((SessionAttributesIdentification) lexResponse.getSessionAttributes())
							.setSecurityQuestionTwo(q.getId());
				}
			} else if (requestSlots.getSecurityQuestionThree() == null) {
				// 1. Create lexResponse
				lexResponse = lex.createElicitSlot("securityQuestionThree", lexRequest, null);

				// 2. Übernahme Session Attribute
				lexResponse.setSessionAttributes(lexRequest.getSessionAttributes());

				// 3. Validierung Question2 (userid, input, frag)
				Boolean val = validierungsService.validate(lexRequest.getUserId(), lexRequest.getInputTranscript(),
						((SessionAttributesIdentification) lexRequest.getSessionAttributes()).getSecurityQuestionTwo());
				LogService.log("Validierung Question2: " + val + "   (Parameter: InputTranscript="
						+ lexRequest.getInputTranscript() + ", securityQuestionTwo="
						+ ((SessionAttributesIdentification) lexRequest.getSessionAttributes())
								.getSecurityQuestionTwo());

				if (!val) {
					// Wieviel mal falsch? Jeder hat 2 Versuche. Danach Question Wechsel. Wieder
					// falsch -> Abbruch.
					// Wurde die SecurityQuestion 1 schonmal falsch beantwortet?
					int securityQuestionTwoAttempt = ((SessionAttributesIdentification) lexResponse
							.getSessionAttributes()).getSecurityQuestionAttemptTwo();
					if (securityQuestionTwoAttempt >= 1) {
						// Send Close Message
						lexResponse = lex.createCloseResponseIdentification(
								"Leider wurden die gestellten Secrityfragen nicht korrekt beantwortet. Melden Sie sich unter 0848 111 444. Sie können die Registrierung auch jederzeit nacholen. Ich wünsche Ihnen noch einen schönen Tag. ",
								"Failed");
					} else {
						// 1.) Attempt auf 1 setzen
						((SessionAttributesIdentification) lexResponse.getSessionAttributes())
								.setSecurityQuestionAttemptTwo(1);

						// 2.) Hole nächste Securityfrage
						QuestionItem q = questionService.getNextQuestion(lexResponse);
						lexResponse.getDialogAction()
								.setMessage(new Message(
										"Ihre Antwort war leider nicht richtig. Bitte versuchen sie die nächste Frage.\n\n"
												+ q.getQuestion()));

						// 3.) Aktualisiere SessionAttribute (securityQuestion1 und actualQuestion)
						((SessionAttributesIdentification) lexResponse.getSessionAttributes())
								.setActualQuestion(q.getId());
						((SessionAttributesIdentification) lexResponse.getSessionAttributes())
								.setSecurityQuestionTwo(q.getId());

						// 4.) Überschreibe SlottoElicit und setze SQ2 wieder auf null
						lexResponse.getDialogAction().setSlotToElicit("securityQuestionTwo");
						((SlotsIdentification) lexResponse.getDialogAction().getSlots()).setSecurityQuestionTwo(null);

						// 5.) Sende lexResponse zurück
					}
				} else {
					// falls die Frage korrekt ist!
					// 1.) Hole nächste Securityfrage
					QuestionItem q = questionService.getNextQuestion(lexResponse);
					lexResponse.getDialogAction().setMessage(new Message(q.getQuestion()));

					// 2.) Aktualisiere SessionAttribute (securityQuestion1 und actualQuestion)
					((SessionAttributesIdentification) lexResponse.getSessionAttributes()).setActualQuestion(q.getId());
					((SessionAttributesIdentification) lexResponse.getSessionAttributes())
							.setSecurityQuestionThree(q.getId());
				}

			} else if (requestSlots.getSecurityQuestionThree() != null
					&& !lexRequest.getSessionAttributes().getIdentified().equals("true")) {
				// 1. Validierung Question3 (userid, input, frag)
				Boolean val = validierungsService.validate(lexRequest.getUserId(), lexRequest.getInputTranscript(),
						((SessionAttributesIdentification) lexRequest.getSessionAttributes())
								.getSecurityQuestionThree());
				LogService.log("Validierung Question3: " + val + "   (Parameter: InputTranscript="
						+ lexRequest.getInputTranscript() + ", securityQuestionThree="
						+ ((SessionAttributesIdentification) lexRequest.getSessionAttributes())
								.getSecurityQuestionThree());

				if (!val) {

					// Wieviel mal falsch? Jeder hat 2 Versuche. Danach Question Wechsel. Wieder
					// falsch -> Abbruch.
					// Wurde die SecurityQuestion 3 schonmal falsch beantwortet?
					int securityQuestionThreeAttempt = ((SessionAttributesIdentification) lexRequest
							.getSessionAttributes()).getSecurityQuestionAttemptThree();
					if (securityQuestionThreeAttempt >= 1) {
						// Send Close Message
						lexResponse = lex.createCloseResponseIdentification(
								"Leider wurden die gestellten Secrityfragen nicht korrekt beantwortet. Melden Sie sich unter 0848 111 444. Sie können die Registrierung auch jederzeit nacholen. Ich wünsche Ihnen noch einen schönen Tag. ",
								"Failed");
					} else {
						// 1.) Create lexResponse
						lexResponse = lex.createElicitSlot("securityQuestionThree", lexRequest, null);

						// 2.) Übernahme Session Attribute
						lexResponse.setSessionAttributes(lexRequest.getSessionAttributes());

						// 3.) Attempt auf 1 setzen
						((SessionAttributesIdentification) lexResponse.getSessionAttributes())
								.setSecurityQuestionAttemptThree(1);

						// 4.) Hole nächste Securityfrage
						QuestionItem q = questionService.getNextQuestion(lexResponse);
						lexResponse.getDialogAction()
								.setMessage(new Message(
										"Ihre Antwort war leider nicht richtig. Bitte versuchen sie die nächste Frage.\n\n "
												+ q.getQuestion()));

						// 5.) Aktualisiere SessionAttribute (securityQuestion1 und actualQuestion)
						((SessionAttributesIdentification) lexResponse.getSessionAttributes())
								.setActualQuestion(q.getId());
						((SessionAttributesIdentification) lexResponse.getSessionAttributes())
								.setSecurityQuestionThree(q.getId());

						// 6.) Setze SQ3 wieder auf null
						((SlotsIdentification) lexResponse.getDialogAction().getSlots()).setSecurityQuestionThree(null);

						// 7.) Sende lexResponse zurück
					}

				} else {
					// 2.) Create Confirmintent ob die Identifizierung gepspeichert werden soll.
					lexResponse = lex.createConfirmIntentIdentifiedSaveYesOrNo(lexRequest);

					// 2.) Übernahme Session Attribute
					lexResponse.setSessionAttributes(lexRequest.getSessionAttributes());

					// 1.) Sezte SessionAttribute identified = true
					lexResponse.getSessionAttributes().setIdentified("true");

					// 7.) Sende lexResponse zurück
				}
			} else if (lexRequest.getSessionAttributes().getIdentified().equals("true")) {

				if (lexRequest.getCurrentIntent().getConfirmationStatus().equals("Confirmed")) {
					// 1.) Save to DB
					db.saveAutoIdentification("true", lexRequest);

					// 2.) Choost intent
					lexResponse = lex.createChooseIntent();

					// 2.) Übernahme Session Attribute
					lexResponse.setSessionAttributes(lexRequest.getSessionAttributes());
					lexResponse.getSessionAttributes().setIdentified("true");

				}

				// Wurde erfoglreich identitifizert. Nur noch testen, ob identifizierung
				// gespeichert werden soll + IntentSwitch auf Payment.
				LogService.log("Frage ob Identifizerung gespeichert werden soll oder nicht");
			}

		}
		return lexResponse;

	}
}
