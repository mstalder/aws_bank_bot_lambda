package com.mst.bankbot.validierung;

import com.mst.bankbot.db.DynamoDBService;
import com.mst.bankbot.db.model.AnswerToQuestionItem;
import com.mst.bankbot.db.model.CorrectAnswerItem;
import com.mst.bankbot.db.model.FalseAnswerItem;
import com.mst.bankbot.db.model.PersonItem;
import com.mst.bankbot.logging.LogService;

public class ValidierungsService {

	private static ValidierungsService instance;
	private DynamoDBService db = DynamoDBService.getInstance();
	
	public static ValidierungsService getInstance() {
		if(instance == null) {
			instance = new ValidierungsService();
		}
		return instance;
	}

	public Boolean validate(String userId, String inputTranscript, int securityQuestion) {
		
		//Hole User von Datenbank
		PersonItem person = db.getUser(userId);
		
		switch(securityQuestion) {
			case 1:{
				return validateQuestion1(person,inputTranscript);
			} 
			case 2:{
				return validateQuestion2(person,inputTranscript);
			}
			case 3:{
				return validateQuestion3(person,inputTranscript);
			}
			case 4:{
				return validateQuestion4(person,inputTranscript);
			}
			case 5:{
				return validateQuestion5(person,inputTranscript);
			}
			case 6:{
				return validateQuestion6(person,inputTranscript);
			}
			case 7:{
				return validateQuestion7(person,inputTranscript);
			}
			case 8:{
				return validateQuestion8(person,inputTranscript);
			}
			case 9:{
				return validateQuestion9(person,inputTranscript);
			}
			case 10:{
				return validateQuestion10(person,inputTranscript);
			}
		
		}
		
		//Frage: Was für Konten führen Sie bei uns?
		LogService.log("keine Validierung fuer Question " + securityQuestion + " gefunden");
		return null;
	}
	
	/**
	 * Auf wenn lautet dieses Konto?
	 * @param person
	 * @param userinput
	 * @return
	 */
	private boolean validateQuestion1(PersonItem person, String userinput) {
		if(userinput.toLowerCase().contains(person.getFirstname().toLowerCase()) || userinput.toLowerCase().contains(person.getLastname().toLowerCase())){
			return true	;
		}
		
		if(userinput.contains("mich")) {
			return true;
		}
		return false;
	}
	
	/**
	 * Wer ist ihr persönlicher Kundenberater?
	 * @param person
	 * @param userinput
	 * @return
	 */
	private boolean validateQuestion2(PersonItem person, String userinput) {
		boolean valid = false;
		AnswerToQuestionItem answerToQuestionItem = person.getAnswerToQuestion(2);
		
		//Loop über alle Antworten, die erfüllt sein müssen
		for(CorrectAnswerItem correctAnswerItem : answerToQuestionItem.getCorrectAnswerList()) {
			
			if(!correctAnswerItem.validateAgainstSynonymList(userinput)) {
				//Wort wurde nicht gefunden!
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Wann haben Sie die Kundenbeziehung mit uns eröffnet?
	 * @param person
	 * @param userinput
	 * @return
	 */
	private boolean validateQuestion3(PersonItem person, String userinput) {
		boolean valid = false;
		AnswerToQuestionItem answerToQuestionItem = person.getAnswerToQuestion(3);
		
		//Loop über alle Antworten, die erfüllt sein müssen
		for(CorrectAnswerItem correctAnswerItem : answerToQuestionItem.getCorrectAnswerList()) {
			
			if(!correctAnswerItem.validateAgainstSynonymList(userinput)) {
				//Wort wurde nicht gefunden!
				return false;
			}
		}
		return true;
		
	}
	
	/**
	 * Was für Konten führen Sie bei uns?
	 * @param person
	 * @param userinput
	 * @return
	 */
	private boolean validateQuestion4(PersonItem person, String userinput) {
		boolean valid = false;
		AnswerToQuestionItem answerToQuestionItem = person.getAnswerToQuestion(4);
		
		//Loop über alle Antworten, die erfüllt sein müssen
		for(CorrectAnswerItem correctAnswerItem : answerToQuestionItem.getCorrectAnswerList()) {
			
			if(!correctAnswerItem.validateAgainstSynonymList(userinput)) {
				//Wort wurde nicht gefunden!
				return false;
			}
		}
		
		//Falls alles True war, über Negativ Antworte loopen
		for(FalseAnswerItem falseAnswerItem : answerToQuestionItem.getFalseAnswertList()) {
			if(falseAnswerItem.validateAgainstSynonymList(userinput)){
				//Wort wurde gefunden
				return false;
			}
		}
		
		return true;
	}

	/**
	 * Wie viele Konten fuehren Sie bei uns?
	 * @param person
	 * @param userinput
	 * @return
	 */
	private boolean validateQuestion5(PersonItem person, String userinput) {
		boolean valid = false;
		AnswerToQuestionItem answerToQuestionItem = person.getAnswerToQuestion(5);
		
		//Loop über alle Antworten, die erfüllt sein müssen
		for(CorrectAnswerItem correctAnswerItem : answerToQuestionItem.getCorrectAnswerList()) {
			
			if(!correctAnswerItem.validateAgainstSynonymList(userinput)) {
				//Wort wurde nicht gefunden!
				return false;
			}
		}
		return true;
		
	}

	/**
	 * Wie hoch ist der aktuelle Kontostand vom genannten Konto
	 * +/- 5000Fr. Darf nur eine Zahl sein, sonst gibt er false zurück.
	 * @param person
	 * @param userinput
	 * @return
	 */
	private boolean validateQuestion6(PersonItem person, String userinput) {
		
		boolean valid = false;
		AnswerToQuestionItem answerToQuestionItem = person.getAnswerToQuestion(6);
		
		double betragInput;
		double betragUser;
		
		try {
			 betragInput = Double.parseDouble(userinput);
			 betragUser = Double.parseDouble(answerToQuestionItem.getCorrectAnswerList().get(0).getAnswer());
		} catch (Exception e) {
			return false;
		}
		
		double untereGrenze = betragUser - 5000;
		double obereGrenze = betragUser + 5000;
		
		if(betragInput >= untereGrenze && betragInput <= obereGrenze) {
			return true;
		}
		
		return false;
	}

	/**
	 * An wen gingen die letzten Zahlungen die sie getätigt haben?
	 * @param person
	 * @param userinput
	 * @return
	 */
	private boolean validateQuestion7(PersonItem person, String userinput) {
		AnswerToQuestionItem answerToQuestionItem = person.getAnswerToQuestion(7);
		
		//Loop über alle Antworten, die erfüllt sein müssen
		for(CorrectAnswerItem correctAnswerItem : answerToQuestionItem.getCorrectAnswerList()) {
			
			if(!correctAnswerItem.validateAgainstSynonymList(userinput)) {
				//Wort wurde nicht gefunden!
				return false;
			}
		}
		return true;
		
	}
	
	/**
	 * Wie hoch war Ihr letzter Bargeldbezug?
//	 * @param person
	 * @param userinput
	 * @return
	 */
	private boolean validateQuestion8(PersonItem person, String userinput) {
		AnswerToQuestionItem answerToQuestionItem = person.getAnswerToQuestion(8);
		
		//Loop über alle Antworten, die erfüllt sein müssen
		for(CorrectAnswerItem correctAnswerItem : answerToQuestionItem.getCorrectAnswerList()) {
			
			if(!correctAnswerItem.validateAgainstSynonymList(userinput)) {
				//Wort wurde nicht gefunden!
				return false;
			}
		}
		return true;
		
	}
	
	/**
	 * Wie lautet die Kartennummer der ***-Karte?
	 * @param person
	 * @param userinput
	 * @return
	 */
	private boolean validateQuestion9(PersonItem person, String userinput) {
		AnswerToQuestionItem answerToQuestionItem = person.getAnswerToQuestion(9);
		
		//Loop über alle Antworten, die erfüllt sein müssen
		for(CorrectAnswerItem correctAnswerItem : answerToQuestionItem.getCorrectAnswerList()) {
			
			if(!correctAnswerItem.validateAgainstSynonymList(userinput)) {
				//Wort wurde nicht gefunden!
				return false;
			}
		}
		return true;
		
	}
	
	/**
	 * Wie hoch ist Ihre Monatslimite der ***-Karte?
	 * @param person
	 * @param userinput
	 * @return
	 */
	private boolean validateQuestion10(PersonItem person, String userinput) {
		AnswerToQuestionItem answerToQuestionItem = person.getAnswerToQuestion(10);
		
		//Loop über alle Antworten, die erfüllt sein müssen
		for(CorrectAnswerItem correctAnswerItem : answerToQuestionItem.getCorrectAnswerList()) {
			
			if(!correctAnswerItem.validateAgainstSynonymList(userinput)) {
				//Wort wurde nicht gefunden!
				return false;
			}
		}
		return true;
		
	}
}
