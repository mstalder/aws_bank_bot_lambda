package com.mst.bankbot.question.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "bankbot-questions")
public class QuestionItem {

	private int id;
	private String question;
	private String securityLevel;
	private String securitygroup;
	private int questionAttempt;

	public QuestionItem() {
		// TODO Auto-generated constructor stub
	}
	
	@DynamoDBHashKey(attributeName = "id")
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@DynamoDBHashKey(attributeName = "question")
	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	@DynamoDBHashKey(attributeName = "securityLevel")
	public String getSecurityLevel() {
		return securityLevel;
	}
	
	
	@DynamoDBHashKey(attributeName = "securitygroup")
	public String getSecuritygroup() {
		return securitygroup;
	}

	public void setSecuritygroup(String securitygroup) {
		this.securitygroup = securitygroup;
	}

	public void setSecurityLevel(String securityLevel) {
		this.securityLevel = securityLevel;
	}

	public int getQuestionAttempt() {
		return questionAttempt;
	}

	public void setQuestionAttempt(int questionAttempt) {
		this.questionAttempt = questionAttempt;
	}

}
