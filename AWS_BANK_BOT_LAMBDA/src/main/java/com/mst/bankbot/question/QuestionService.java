package com.mst.bankbot.question;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.StringTokenizer;

import com.mst.bankbot.db.DynamoDBService;
import com.mst.bankbot.lex.model.LexRespond;
import com.mst.bankbot.lex.model.identification.SessionAttributesIdentification;
import com.mst.bankbot.question.model.QuestionItem;

public class QuestionService {
	
	private static QuestionService instance;
	private DynamoDBService db = DynamoDBService.getInstance();
	private List<QuestionItem> questionAllList;

	public static QuestionService getInstance() {
		if(instance == null) {
			instance = new QuestionService();
			//get All Questions from DB.
			instance.setQuestionList(instance.getDb().getAllSecurityQuestions());
		}
		return instance;
	}
	
	
	/**
	 * Holt alle Fragen aus der Datenbank.
	 * @return
	 */
	public List<QuestionItem> getAllQuestions(){
		return questionAllList;
	}
	
	public QuestionItem getQuestion(int id) {
		return new QuestionItem();
	}
	
	public QuestionItem getQuestion(String securityLevel) {
		return new QuestionItem();
	}
	
	/**
	 * Get a question from on all Groups and securitylevel
	 * @param lexRequest
	 * @return
	 */
	public QuestionItem getQuestionRandom() {
		int size = getAllQuestions().size();
		
		QuestionItem questionItem = db.getQuestionWithId(wuerfel(size));
		
		return questionItem;
	}


	public List<QuestionItem> getQuestionList() {
		return questionAllList;
	}


	public void setQuestionList(List<QuestionItem> questionList) {
		this.questionAllList = questionList;
	}


	public DynamoDBService getDb() {
		return db;
	}


	public void setDb(DynamoDBService db) {
		this.db = db;
	}
	
	
	public int wuerfel(int max) {
		Random random = new Random();
		return (1 + random.nextInt(max));
	}


	/**
	 * Holt die nächste SecurityFrage. Gestellt Fragen werden ausgelassen und es wird eine Frage aus einer anderen Kategorie gestellt.
	 * @param lexResponse
	 * @return
	 */
	public QuestionItem getNextQuestion(LexRespond lexResponse) {
		//1.) Welche Question sind bereits gestellt worden?
		ArrayList<Integer> questionNotToAskAnymoreList = new ArrayList<Integer>(); 
		
		//Ignore ActualQuestion
		int actualQuestion = ((SessionAttributesIdentification) lexResponse.getSessionAttributes()).getActualQuestion();
		questionNotToAskAnymoreList.add(actualQuestion);

		//Ignore securityQuestion1
		int securityQuestion1 =  ((SessionAttributesIdentification) lexResponse.getSessionAttributes()).getSecurityQuestionOne(); 
		if(securityQuestion1 != 0) {
			questionNotToAskAnymoreList.add(securityQuestion1);
		}
		
		//Ignore securityQuestion2
		int securityQuestion2 =  ((SessionAttributesIdentification) lexResponse.getSessionAttributes()).getSecurityQuestionTwo(); 
		if(securityQuestion2 != 0) {
			questionNotToAskAnymoreList.add(securityQuestion2);
		}
		
		//Ignore securityQuestion3
		int securityQuestion3 =  ((SessionAttributesIdentification) lexResponse.getSessionAttributes()).getSecurityQuestionThree(); 
		if(securityQuestion3 != 0) {
			questionNotToAskAnymoreList.add(securityQuestion3);
		}
		
		//Ignore questionFailed
		String questionFailed = ((SessionAttributesIdentification) lexResponse.getSessionAttributes()).getQuestionFailed();
		if(questionFailed != null) {
			StringTokenizer stringTokenizerQuestionFailed = new StringTokenizer(questionFailed, ",");
			while(stringTokenizerQuestionFailed.hasMoreElements()) {
				questionNotToAskAnymoreList.add(Integer.valueOf(stringTokenizerQuestionFailed.nextElement().toString()));		
			}
		}

		//welche ist die nächste Frage?
		QuestionItem questionItem = getQuestionWithFilter(questionNotToAskAnymoreList,db.getQuestionWithId(actualQuestion).getSecurityLevel());
		
		//Hole Question in DB ohne Liste
		
		
		return questionItem;
	}


	public QuestionItem getQuestionWithFilter(ArrayList<Integer> questionNotToAskAnymoreList, String securityLevel) {
		//ArrayList<QuestionItem> questionItemList = db.getQuestionWithFilter(questionNotToAskAnymoreList, securityLevel);
		ArrayList<QuestionItem> questionAvailableList = new ArrayList<QuestionItem>();
		
		//Füge nur Fragen hinzu, welche ein anderes SecurityLevel haben und nicht bereits abgefragt wurden
		for(QuestionItem qItem : getQuestionList()) {
			if(!qItem.getSecurityLevel().equals(securityLevel)) {
				
				//Ist die Frage bereits gestellt worden?
				Boolean alreadyAsked = false;
				for (int i : questionNotToAskAnymoreList) {
					if(qItem.getId() == i) {
						//Frage ist bereits gestellt worden
						alreadyAsked = true;
					}
				}
				
				//Falls Frage noch nicht gestellt wurde, dann hinzufügen zu den möglichen Antworten.
				if(!alreadyAsked) {
					questionAvailableList.add(qItem);
				}
			}
		}
		
		//wuerfle und gib Frage zurück.
		int questionNumberForReturn= wuerfel(questionAvailableList.size()-1);
		
	
		return questionAvailableList.get(questionNumberForReturn);
	}


	
}
