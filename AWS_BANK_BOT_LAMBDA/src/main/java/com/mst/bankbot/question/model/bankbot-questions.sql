
INSERT INTO bankbot-questions (id, securitygroup, securityLevel, question) VALUES ('1', 'Kundenbeziehung', 'tief', 'Auf wen lautet dieses Konto?' );
INSERT INTO bankbot-questions (id, securitygroup, securityLevel, question) VALUES ('2', 'Kundenbeziehung', 'hoch', 'Wer ist ihr persönlicher Kundenberater?' );
INSERT INTO bankbot-questions (id, securitygroup, securityLevel, question) VALUES ('3', 'Kundenbeziehung', 'hoch', 'Wann haben Sie die Kundenbeziehung mit uns eröffnet?' );

INSERT INTO bankbot-questions (id, securitygroup, securityLevel, question) VALUES ('4', 'Konten', 'mittel', 'Was für Konten führen Sie bei uns?' );
INSERT INTO bankbot-questions (id, securitygroup, securityLevel, question) VALUES ('5', 'Konten', 'mittel', 'Wieviele Konten führen Sie bei uns?' );
INSERT INTO bankbot-questions (id, securitygroup, securityLevel, question) VALUES ('6', 'Konten', 'mittel', 'Wie hoch ist der aktuelle Kontostand vom genannten Konto?' );


INSERT INTO bankbot-questions (id, securitygroup, securityLevel, question) VALUES ('7', 'Kontobewegungen', 'hoch', 'An wen ging die letzte Zahlung die Sie getätigt haben?' );
INSERT INTO bankbot-questions (id, securitygroup, securityLevel, question) VALUES ('8', 'Kontobewegungen', 'mittel', 'Wie hoch war Ihr letzter Bargeldbezug?' );

INSERT INTO bankbot-questions (id, securitygroup, securityLevel, question) VALUES ('9', 'Diensleistung', 'mittel', 'Wie lautet die Kartennummer Ihrer Kreditkarte?' );
INSERT INTO bankbot-questions (id, securitygroup, securityLevel, question) VALUES ('10', 'Diensleistung', 'tief', 'Wie hoch ist Ihre Monatslimite Ihrer Kredikarte' );



select * from bankbot-questions;