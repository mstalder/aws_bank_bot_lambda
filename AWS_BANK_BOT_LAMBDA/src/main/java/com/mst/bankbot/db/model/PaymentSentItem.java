package com.mst.bankbot.db.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;

@DynamoDBDocument
public class PaymentSentItem {

	private String to;
	private int amount;
	private String sentDate;

	public PaymentSentItem() {
		// Default Constructor
	}

	public PaymentSentItem(String to, int amount, String sentDate) {
		super();
		this.to = to;
		this.amount = amount;
		this.sentDate = sentDate;
	}

	public String getSentDate() {
		return sentDate;
	}

	public void setSentDate(String sentDate) {
		this.sentDate = sentDate;
	}

	@DynamoDBAttribute(attributeName = "to")
	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	@DynamoDBAttribute(attributeName = "amount")
	public int getAmount() {
		return amount;
	}

	@DynamoDBAttribute(attributeName = "sentDate")
	public void setAmount(int amount) {
		this.amount = amount;
	}

}
