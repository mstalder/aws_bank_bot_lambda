package com.mst.bankbot.db.model;

import java.util.ArrayList;
import java.util.Date;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "bankbot-users")
public class PersonItem {

	private String userid;
	private String firstname;
	private String lastname;
	private String identifed;
	
	private ArrayList<AnswerToQuestionItem> answerToQuestionList = new ArrayList<AnswerToQuestionItem>();
	private ArrayList<PaymentReceiveItem> paymentReceiveItemList = new ArrayList<PaymentReceiveItem>();
	private ArrayList<PaymentSentItem> paymentSentItemList = new ArrayList<PaymentSentItem>();

	public PersonItem() {
		// TODO Auto-generated constructor stub
	}
	
	public PersonItem(String userid, String firstname, String lastname) {
		this.userid = userid;
		this.firstname = firstname;
		this.lastname = lastname;
	}
	
	@DynamoDBHashKey(attributeName = "userid")
	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	@DynamoDBAttribute(attributeName = "firstname")
	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	@DynamoDBAttribute(attributeName = "lastname")
	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	@DynamoDBAttribute(attributeName = "answerToQuestionList")
	public ArrayList<AnswerToQuestionItem> getAnswerToQuestionList() {
		return answerToQuestionList;
	}

	public void setAnswerToQuestionList(ArrayList<AnswerToQuestionItem> answerToQuestionList) {
		this.answerToQuestionList = answerToQuestionList;
	}

	
	@DynamoDBAttribute(attributeName = "paymentReceiveItemList")
	public ArrayList<PaymentReceiveItem> getPaymentReceiveItemList() {
		return paymentReceiveItemList;
	}

	public void setPaymentReceiveItemList(ArrayList<PaymentReceiveItem> paymentReceiveItemList) {
		this.paymentReceiveItemList = paymentReceiveItemList;
	}

	@DynamoDBAttribute(attributeName = "paymentSentItemList")
	public ArrayList<PaymentSentItem> getPaymentSentItemList() {
		return paymentSentItemList;
	}

	public void setPaymentSentItemList(ArrayList<PaymentSentItem> paymentSentItemList) {
		this.paymentSentItemList = paymentSentItemList;
	}

	
	public void addAnswerToQuestionList(AnswerToQuestionItem answerToQuestionItem) {
		this.answerToQuestionList.add(answerToQuestionItem);
	}

	public void addPaymentReceiveItem(String from, int amount, String receiveDate) {
		getPaymentReceiveItemList().add(new PaymentReceiveItem(from, amount, receiveDate));
	}
	
	public void addPaymentSentItem(String to, int amount, String sentDate) {
		getPaymentSentItemList().add(new PaymentSentItem(to, amount, sentDate));
	}
	
	public AnswerToQuestionItem getAnswerToQuestion(int questionId) {
		for(AnswerToQuestionItem answerToQuestion : this.getAnswerToQuestionList()) {
			if(answerToQuestion.getQuestionId() == questionId) {
				return answerToQuestion;
			}
		}
		return null;
	}

	public String getIdentifed() {
		return identifed;
	}

	public void setIdentifed(String identifed) {
		this.identifed = identifed;
	}


	

}
