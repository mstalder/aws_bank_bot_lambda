package com.mst.bankbot.db.model;

import java.util.ArrayList;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;

@DynamoDBDocument
public class CorrectAnswerItem {

	private String answer;
	private ArrayList<String> synonymPossibleAnswerList = new ArrayList<String>();

	public CorrectAnswerItem() {
		//default constructor
	}
	
	public CorrectAnswerItem(String answer) {
		this.answer = answer;
	}

	@DynamoDBAttribute(attributeName = "answer")
	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	@DynamoDBAttribute(attributeName = "getSynonymPossibleAnswerList")
	public ArrayList<String> getSynonymPossibleAnswerList() {
		return synonymPossibleAnswerList;
	}

	public void setSynonymPossibleAnswerList(ArrayList<String> synonymPossibleAnswerList) {
		this.synonymPossibleAnswerList = synonymPossibleAnswerList;
	}

	public void addPossibleAnswer(String synonymPossibleAnswer) {
		this.synonymPossibleAnswerList.add(synonymPossibleAnswer);
	}

	/**
	 * true = falls Wort in Synyomliste gefunden wird
	 * false = falls Wort nicht vorkommt!
	 * @param userinput
	 * @return
	 */
	public boolean validateAgainstSynonymList(String userinput) {
		boolean found = false;
		for(String s : this.getSynonymPossibleAnswerList()) {
			if(userinput.toLowerCase().contains(s.toLowerCase())) {
				found = true;
			}
		}
		return found;
	}
}
