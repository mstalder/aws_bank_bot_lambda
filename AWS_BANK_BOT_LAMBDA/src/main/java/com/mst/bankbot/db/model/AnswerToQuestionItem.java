package com.mst.bankbot.db.model;

import java.util.ArrayList;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;

@DynamoDBDocument
public class AnswerToQuestionItem {

	private int questionId;
	private ArrayList<CorrectAnswerItem> correctAnswerList = new ArrayList<CorrectAnswerItem>();
	private ArrayList<FalseAnswerItem> falseAnswertList = new ArrayList<FalseAnswerItem>();

	public AnswerToQuestionItem() {
		// default Constructor
	}
	
	public AnswerToQuestionItem(int questionId) {
		this.questionId = questionId;
	}

	@DynamoDBAttribute(attributeName = "questionId")
	public int getQuestionId() {
		return questionId;
	}

	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}

	@DynamoDBAttribute(attributeName = "correctAnswerList")
	public ArrayList<CorrectAnswerItem> getCorrectAnswerList() {
		return correctAnswerList;
	}

	public void setCorrectAnswerList(ArrayList<CorrectAnswerItem> correctAnswerList) {
		this.correctAnswerList = correctAnswerList;
	}
	
	
	@DynamoDBAttribute(attributeName = "falseAnswerList")
	public ArrayList<FalseAnswerItem> getFalseAnswertList() {
		return falseAnswertList;
	}

	public void setFalseAnswertList(ArrayList<FalseAnswerItem> falseAnswertList) {
		this.falseAnswertList = falseAnswertList;
	}

	public void addCorrectAnswer(CorrectAnswerItem correctAnswerItem) {
		this.correctAnswerList.add(correctAnswerItem);
	}
	
	public void addFalseAnswer(FalseAnswerItem falseAnswerItem) {
		this.falseAnswertList.add(falseAnswerItem);
	}
}