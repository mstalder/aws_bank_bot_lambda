package com.mst.bankbot.db;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.mst.bankbot.db.model.PaymentReceiveItem;
import com.mst.bankbot.db.model.PaymentSentItem;
import com.mst.bankbot.db.model.PersonItem;
import com.mst.bankbot.lex.model.LexRequest;
import com.mst.bankbot.question.model.QuestionItem;

public class DynamoDBService {

	private static DynamoDBService instance;
	private DynamoDBMapper mapper;
	
	public static DynamoDBService getInstance() {
		if(instance == null) {
			instance = new DynamoDBService();
			
			AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().build();
			DynamoDB dynamoDB = new DynamoDB(client);
			instance.mapper = new DynamoDBMapper(client);			
		}
		return instance;
	}
	
	/**
	 * Stores a new User into database
	 * @param person
	 * @return
	 */
	public boolean createUser(PersonItem person) {
		mapper.save(person);
		return true;
	}
	
	
	/**
	 * Get User from database
	 * @param facebookUserID
	 * @return
	 */
	public PersonItem getUser(String facebookUserID) {
		PersonItem person = mapper.load(PersonItem.class,facebookUserID);
		return person;
	}

	/**
	 * Updates a user in database
	 * @param person
	 * @return
	 */
	public boolean updateUser(PersonItem person) {		
		mapper.save(person);
		return true;
	}
	
	public boolean deleteUser(PersonItem person) {
		return true;
	}
	
	public boolean deleteUser(String facebookUserID) {
		return true;
	}
	
	
	/**
	 * Checks if the user exists in the dynamodb.
	 * @param facebookUserId
	 * @return true/false
	 */
	public boolean userExists(String facebookUserId) {
		PersonItem p = new PersonItem();
		p.setUserid(facebookUserId);
		DynamoDBQueryExpression<PersonItem> queryExpression = new DynamoDBQueryExpression<PersonItem>().withHashKeyValues(p);
		List<PersonItem> result = mapper.query(PersonItem.class, queryExpression);
		if (result.size() > 0) {
			return true;
		}
		return false;
	}

	public boolean isAlreadyAuthenticated(String userId) {

		if(getUser(userId) != null && getUser(userId).getIdentifed().equals("true")){
			return true;
		} else {
			return false;
		}	
		
	}

	public List<QuestionItem> getAllSecurityQuestions() {
		
		Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
		eav.put(":val1", new AttributeValue().withS("tief"));
		eav.put(":val2", new AttributeValue().withS("mittel"));
		eav.put(":val3", new AttributeValue().withS("hoch"));
		
		DynamoDBScanExpression scanExpression = new DynamoDBScanExpression().withFilterExpression("securityLevel in(:val1,:val2,:val3)").withExpressionAttributeValues(eav);;
		List<QuestionItem> scanResult = mapper.scan(QuestionItem.class, scanExpression);
				
//	    for (QuestionItem questionItem : scanResult) {
//	            System.out.println(questionItem.getQuestion());
//	    }

		return scanResult;
	}

	public QuestionItem getQuestionWithId(int questionID) {
		
		Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
		eav.put(":val1", new AttributeValue().withN(String.valueOf(questionID)));
		
		DynamoDBScanExpression scanExpression = new DynamoDBScanExpression().withFilterExpression("id = :val1").withExpressionAttributeValues(eav);;
		List<QuestionItem> scanResult = mapper.scan(QuestionItem.class, scanExpression);
		
		return scanResult.get(0);
	}

	public void saveAutoIdentification(String autoSave, LexRequest lexRequest) {
		PersonItem p = getUser(lexRequest.getUserId());
		p.setIdentifed(autoSave);
		mapper.save(p);
	}

	/**
	 * Differenz +- 10 Tage und 50Fr. 
	 * @param userId
	 * @param amount
	 * @param date
	 * @return
	 */
	public ArrayList<PaymentReceiveItem> getAllPaymentReceivedFromPerson(String userId, String amount, String date) {
		ArrayList<PaymentReceiveItem> resultList = new ArrayList<PaymentReceiveItem>();
		PersonItem p = getUser(userId);
		
		
		//Beträge
		int diffBetrag = 51;
		double betrag = Integer.valueOf(amount);
		double betragMinimum = betrag - diffBetrag;
		double betragMaximum = betrag + diffBetrag;
		
		//CheckDate
		SimpleDateFormat formatter1=new SimpleDateFormat("dd.MM.yyyy");
		
		for(PaymentReceiveItem payment : p.getPaymentReceiveItemList()) {
			if(betragMaximum >= payment.getAmount() && payment.getAmount() >= betragMinimum) {
				
				try {
					Date userDate = formatter1.parse(date);                      
					Date dbDate = formatter1.parse(payment.getReceiveDate());    
					Date minDate = removeDaystoDate(userDate, 11);
					Date maxDate = addDaystoDate(userDate, 11);
			        
//					System.out.println("minDate: " + minDate);
//					System.out.println("maxDate: " + maxDate);
//					System.out.println("dbDate: " + dbDate);
					if(dbDate.after(minDate) && dbDate.before(maxDate)) {
						resultList.add(payment);
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}  

			}				
		}
		
		return resultList;
	}

	/**
	 * Differenz 5Fr / + 10Tage
	 * @param userId
	 * @param amount
	 * @param date
	 * @return
	 */
	public ArrayList<PaymentSentItem> getAllPaymentSentFromPerson(String userId, String amount, String date) {
		ArrayList<PaymentSentItem> resultList = new ArrayList<PaymentSentItem>();
		PersonItem p = getUser(userId);
		
		//Beträge
		int diffBetrag = 31;
		double betrag = Integer.valueOf(amount);
		double betragMinimum = betrag - diffBetrag;
		double betragMaximum = betrag + diffBetrag;
		
		//Datum
		SimpleDateFormat formatter1=new SimpleDateFormat("dd.MM.yyyy");		
		
		for(PaymentSentItem payment : p.getPaymentSentItemList()) {
			if(betragMaximum >= payment.getAmount() && payment.getAmount() >= betragMinimum) {
				
				//CheckDate
				try {
					Date userDate = formatter1.parse(date);                     
					Date dbDate = formatter1.parse(payment.getSentDate());   
					Date minDate = removeDaystoDate(userDate, 6);
					Date maxDate = addDaystoDate(userDate, 6);
					
					if(dbDate.after(minDate) && dbDate.before(maxDate)) {
						resultList.add(payment);
					}
				} catch (ParseException e) {
					e.printStackTrace();
				}  

			}				
		}
		
		return resultList;
	}
	
	private Date addDaystoDate (Date date, int daysToAdd) {
		Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, daysToAdd);
		
        return c.getTime();
	}
	
	private Date removeDaystoDate (Date date, int daysToRemove) {
		Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, -daysToRemove);
		
        return c.getTime();
	}

//	public ArrayList<QuestionItem> getQuestionWithFilter(ArrayList<Integer> questionNotToAskAnymoreList, String securityLevel) {
//		ArrayList<QuestionItem> results = null;
//		
//		//SecurityLevel
//		Map<String, AttributeValue> eav = new HashMap<String, AttributeValue>();
//		if(!securityLevel.equals("tief")) {
//			eav.put(":val1", new AttributeValue().withS("tief"));	
//		}
//		if(!securityLevel.equals("mittel")) {
//			eav.put(":val2", new AttributeValue().withS("mittel"));	
//		}
//		if(!securityLevel.equals("hoch")) {
//			eav.put(":val3", new AttributeValue().withS("hoch"));	
//		}
//		
////		//QuestionToIgnore
////		Map<String, AttributeValue> questionIgnore = new HashMap<String, AttributeValue>();
////		for(int id : questionNotToAskAnymoreList) {
////			
////		}
//		
//		//DynamoDBScanExpression scanExpression = new DynamoDBScanExpression().withFilterExpression("securityLevel in(mittel)");
//		DynamoDBScanExpression scanExpression = new DynamoDBScanExpression().withSelect("securityLevel,question,securitygroup,id from bankbot-questions");
//		//DynamoDBScanExpression scanExpression = new DynamoDBScanExpression().withSelect("select securityLevel,question,securitygroup,id from bankbot-questions");
//		
//
//		//New
//		Table table = DynamoDBService.
//		
//		//--------------
//		
//		
//		List<QuestionItem> scanResult = mapper.scan(QuestionItem.class, scanExpression);
//		
//	    for (QuestionItem questionItem : scanResult) {
//	    	System.out.println(questionItem.getQuestion());
//	    }
//		
//		return results;
//	}
	
	

}
