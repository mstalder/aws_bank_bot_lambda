package com.mst.bankbot.db.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;

@DynamoDBDocument
public class PaymentReceiveItem {

	private String from;
	private int amount;
	private String receiveDate;

	public PaymentReceiveItem() {
		// Default Constructor
	}

	public PaymentReceiveItem(String from, int amount, String receiveDate) {
		super();
		this.from = from;
		this.amount = amount;
		this.receiveDate = receiveDate;
	}

	@DynamoDBAttribute(attributeName = "from")
	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	@DynamoDBAttribute(attributeName = "amount")
	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	@DynamoDBAttribute(attributeName = "receiveDate")
	public String getReceiveDate() {
		return receiveDate;
	}

	public void setReceiveDate(String receiveDate) {
		this.receiveDate = receiveDate;
	}

}
