package com.mst.bankbot.db.model;

import java.util.ArrayList;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;

@DynamoDBDocument
public class FalseAnswerItem {

	private String answer;
	private ArrayList<String> synonymPossibleFalseAnswerList = new ArrayList<String>();

	public FalseAnswerItem() {
		//default constructor
	}
	
	public FalseAnswerItem(String answer) {
		this.answer = answer;
	}

	@DynamoDBAttribute(attributeName = "answer")
	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	@DynamoDBAttribute(attributeName = "getSynonymFalsePossibleAnswerList")
	public ArrayList<String> getSynonymPossibleFalseAnswerList() {
		return synonymPossibleFalseAnswerList;
	}

	public void setSynonymPossibleFalseAnswerList(ArrayList<String> synonymPossibleFalseAnswerList) {
		this.synonymPossibleFalseAnswerList = synonymPossibleFalseAnswerList;
	}

	public void addPossibleFalseAnswer(String synonymPossibleFalseAnswer) {
		this.synonymPossibleFalseAnswerList.add(synonymPossibleFalseAnswer);
	}

	/**
	 * true => Falls Wort gefunden wird 
	 * @param userinput
	 * @return
	 */
	public boolean validateAgainstSynonymList(String userinput) {
		boolean found = false;
		for(String s : this.getSynonymPossibleFalseAnswerList()) {
			if(userinput.toLowerCase().contains(s.toLowerCase())) {
				found = true;
			}
		}
		return found;
	}
}
