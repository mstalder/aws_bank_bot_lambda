package com.mst.bankbot.logging;

import com.amazonaws.services.lambda.runtime.Context;

public class LogService {

	private static Context context;

	public static Context getContext() {
		return context;
	}

	public static void setContext(Context context) {
		LogService.context = context;
	}

	public static void log(String message) {
		context.getLogger().log(message);
	}

}
