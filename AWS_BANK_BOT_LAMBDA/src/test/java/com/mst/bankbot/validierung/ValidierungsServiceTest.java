package com.mst.bankbot.validierung;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

public class ValidierungsServiceTest {

	private static ValidierungsService val;
	private static String facebookUserId;
	
	
    @BeforeClass
    public static void setupTestEnviroment() throws IOException {
        // TODO: set up your sample input object here.
        val = ValidierungsService.getInstance();
        facebookUserId = "1963775793750586";
    }
	
    /**
     * Auf wen lautet dieses Konto?
     */
	@Test
	public void question1Test() {
		
		assertTrue(val.validate(facebookUserId, "Margrit Fehr", 1));
		assertFalse(val.validate(facebookUserId, "Margrit Gugus", 1));
		assertFalse(val.validate(facebookUserId, "Gugus Fehr", 1));
	}
	
	/**
	 * Wer ist ihr persönlicher Kundenberater?
	 */
	@Test
	public void question2Test() {
		
		assertTrue(val.validate(facebookUserId, "Michael Kuhn", 2));
		assertTrue(val.validate(facebookUserId, "Michael", 2));
		assertTrue(val.validate(facebookUserId, "Kuhn", 2));
		assertFalse(val.validate(facebookUserId, "Fabian Schweizer", 2));
		assertFalse(val.validate(facebookUserId, "Flurin", 2));
	}
	
	/**
	 * Wann haben Sie die Kundenbeziehung mit uns eröffnet?
	 */
	@Test
	public void question3Test() {
		
		assertTrue(val.validate(facebookUserId, "24.11.2001", 3));
		assertTrue(val.validate(facebookUserId, "2001", 3));
		assertTrue(val.validate(facebookUserId, "November", 3));
		assertFalse(val.validate(facebookUserId, "Dezember", 3));
		assertFalse(val.validate(facebookUserId, "21.05.17", 3));
		
	}
    
	/**
	 * Was für Konten führen Sie bei uns?
	 */
	@Test
	public void question4Test() {
		ArrayList<String> userInputListValid = new ArrayList<String>();
		userInputListValid.add("Ein Sparkonto und ein Privatkonto");
		userInputListValid.add("Ein Spar und ein Privatkonto");
		userInputListValid.add("Ein Privat und Sparkonto");
		
		boolean valid;
		for(String inputTranscript : userInputListValid) {
			valid = val.validate(facebookUserId, inputTranscript, 4);
			System.out.println(inputTranscript + ": " + valid);
			assertEquals(true, valid);
		}
	}
	
	/**
	 * Was für Konten führen Sie bei uns?
	 */
	@Test
	public void question4FalseTest() {
		ArrayList<String> userInputListValid = new ArrayList<String>();
		userInputListValid.add("Ich habe ein Sparkonto");
		userInputListValid.add("Ich habe ein Spar und ein Anlagekonto");
		
		boolean valid;
		for(String inputTranscript : userInputListValid) {
			valid = val.validate(facebookUserId, inputTranscript, 4);
			System.out.println(inputTranscript + ": " + valid);
			assertEquals(false, valid);
		}
		
	}
	
	/**
	 * Wie viele Konten führen Sie bei uns?
	 */
	@Test
	public void question5Test() {
		int questionId = 5;
		assertTrue(val.validate(facebookUserId, "2", questionId));
		assertTrue(val.validate(facebookUserId, "zwei", questionId));
		assertFalse(val.validate(facebookUserId, "drei", questionId));
		assertFalse(val.validate(facebookUserId, "3", questionId));
	}
	
	/**
	 * Wie hoch ist der aktuelle Kontostand vom genannten Konto?
	 */
	@Test
	public void question6Test() {
		int questionId = 6;
		assertTrue(val.validate(facebookUserId, "20000", questionId));
		assertTrue(val.validate(facebookUserId, "21500", questionId));
		assertTrue(val.validate(facebookUserId, "15000", questionId));
		assertTrue(val.validate(facebookUserId, "25000", questionId));
		assertFalse(val.validate(facebookUserId, "14999", questionId));
		assertFalse(val.validate(facebookUserId, "25010", questionId));
		assertFalse(val.validate(facebookUserId, "ein Text mit nummer 20000", questionId));
	}
	
	/**
	 * An wen gingen die letzten Zahlungen die sie getätigt haben?
	 */
	@Test
	public void question7Test() {
		int questionId = 7;
		assertTrue(val.validate(facebookUserId, "Klaus Heckmann", questionId));
		assertTrue(val.validate(facebookUserId, "Klaus", questionId));
		assertTrue(val.validate(facebookUserId, "Heckmann", questionId));
		assertFalse(val.validate(facebookUserId, "Lukas Oberholzer", questionId));
		assertFalse(val.validate(facebookUserId, "Fabian ", questionId));
		assertFalse(val.validate(facebookUserId, "Herr Graf", questionId));
	}
	
	/**
	 * Wie hoch war Ihr letzter Bargeldbezug?
	 */
	@Test
	public void question8Test() {
		int questionId = 8;
		assertTrue(val.validate(facebookUserId, "100", questionId));
		assertTrue(val.validate(facebookUserId, "100Fr.", questionId));
		assertTrue(val.validate(facebookUserId, "100.-", questionId));
		assertFalse(val.validate(facebookUserId, "80", questionId));
		assertFalse(val.validate(facebookUserId, "50000", questionId));
	}
	
	/**
	 * Wie lautet die Kartennummer der ***-Karte?
	 */
	@Test
	public void question9Test() {
		int questionId = 9;
		assertTrue(val.validate(facebookUserId, "5502 3574 0150 8102", questionId));
		assertTrue(val.validate(facebookUserId, "5502357401508102", questionId));
		assertFalse(val.validate(facebookUserId, "6702 4818 0567 8103", questionId));
		assertFalse(val.validate(facebookUserId, "6702481805678103", questionId));
	}
	
	/**
	 * Wie hoch ist Ihre Monatslimite der ***-Karte?
	 */
	@Test
	public void question10Test() {
		int questionId = 10;
		assertTrue(val.validate(facebookUserId, "5000", questionId));
		assertFalse(val.validate(facebookUserId, "6000", questionId));
		assertFalse(val.validate(facebookUserId, "2000", questionId));
	}
	

}
