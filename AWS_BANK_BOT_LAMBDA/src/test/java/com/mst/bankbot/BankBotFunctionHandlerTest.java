package com.mst.bankbot;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.FileUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * A simple test harness for locally invoking your Lambda function handler.
 */
public class BankBotFunctionHandlerTest {

	private static TestContext ctx;
	private static BankBotFunctionHandler handler;

    @BeforeClass
    public static void createInput() throws IOException {
    	ctx = new TestContext();
    	ctx.setFunctionName("BankBotFunctionHandler");
    	handler = new BankBotFunctionHandler();
    }



	/**
	 * IntenSwitch Test von Payment auf Identification Intent
	 * @throws IOException
	 */
	@Test
	public void BankBotFunctionHandlerRequest1Init() throws IOException {

		InputStream input = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("src/test/resources/requests/Payment-Request1-init.json")));
		OutputStream output = new ByteArrayOutputStream();

		handler.handleRequest(input, output, ctx);
		
		//In JSON Objekt wandeln.
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonOutput = output.toString();
		JsonNode rootNode = objectMapper.readTree(jsonOutput);

		assertEquals("Du hast aktuell keine Berechtigung Account Abfragen zu tätigen", rootNode.at("/dialogAction/message/content").asText());
	}
	
	
	/**
	 * Identification Start Request
	 * @throws IOException
	 */
	@Test
	public void IdentificationRequestInputAskSq1() throws IOException {

		InputStream input = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("src/test/resources/requests/Identification-RequestInput-AskSq1.json")));
		OutputStream output = new ByteArrayOutputStream();
		
		handler.handleRequest(input, output, ctx);
		
		//In JSON Objekt wandeln.
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonOutput = output.toString();
		JsonNode rootNode = objectMapper.readTree(jsonOutput);
		
		
		assertEquals("securityQuestionOne", rootNode.at("/dialogAction/slotToElicit").asText());
		assertEquals(String.valueOf("null"),rootNode.at("/dialogAction/slots/securityQuestionOne").asText());
		
	}
	
	
	/**
	 * Identification-RequestInput-AskSq2-ValSq1-positiv.json
	 * Erwartet: ElicitSlot für seucrityQuestionTwo
	 * @throws IOException
	 */
	@Test
	public void IdentificationRequestInputAskSq2ValSq1Positiv() throws IOException {

		InputStream input = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("src/test/resources/requests/Identification-RequestInput-AskSq2-ValSq1-positiv.json")));
		OutputStream output = new ByteArrayOutputStream();
		
		handler.handleRequest(input, output, ctx);
		
		//In JSON Objekt wandeln.
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonOutput = output.toString();
		JsonNode rootNode = objectMapper.readTree(jsonOutput);
		
		assertEquals("securityQuestionTwo", rootNode.at("/dialogAction/slotToElicit").asText());
		assertEquals("PlainText", rootNode.at("/dialogAction/message/contentType").asText());
		assertNotNull(rootNode.at("/dialogAction/slots/securityQuestionOne").asText());
		
	}
	
	
	/**
	 * Identification-RequestInput-SecurityQuestion2-negativ     (nur Sparkonto -> Ist eines zu wenig)
	 * Problem: 	Nur Sparkonto angegeben, obwohl sie Spar und Privatkonto hat.
	 * Testinput:	Identification-RequestInput-SecurityQuestion2-negativ.json
	 * Output:		
	 * @throws IOException
	 */
	@Test
	public void IdentificationRequestInputAskSq2ValSq1Negativ() throws IOException {

		InputStream input = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("src/test/resources/requests/Identification-RequestInput-AskSq2-ValSq1-negativ.json")));
		OutputStream output = new ByteArrayOutputStream();
		
		handler.handleRequest(input, output, ctx);
		
		//In JSON Objekt wandeln.
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonOutput = output.toString();
		JsonNode rootNode = objectMapper.readTree(jsonOutput);
		
		assertEquals(1, rootNode.at("/sessionAttributes/securityQuestionAttemptOne").asInt());
		assertEquals(String.valueOf("null"),rootNode.at("/dialogAction/slots/securityQuestionOne").asText());
		

	}
	
	/**
	 * Identification-RequestInput-SecurityQuestion2-negativ     (nur Sparkonto -> Ist eines zu wenig)
	 * Problem: 	SecurityQuestion1 ist 2x falsch beantwortet.
	 * Testinput:	Identification-RequestInput-AskSq2-ValSq1-negativ2.json
	 * Output:		Close Meldung.
	 * @throws IOException
	 */
	@Test
	public void IdentificationRequestInputAskSq2ValSq1Negativ2() throws IOException {

		InputStream input = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("src/test/resources/requests/Identification-RequestInput-AskSq2-ValSq1-negativ2.json")));
		OutputStream output = new ByteArrayOutputStream();
		
		handler.handleRequest(input, output, ctx);
		
		//In JSON Objekt wandeln.
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonOutput = output.toString();
		JsonNode rootNode = objectMapper.readTree(jsonOutput);
		
		assertEquals("Close", rootNode.at("/dialogAction/type").asText());
		assertEquals("Failed", rootNode.at("/dialogAction/fulfillmentState").asText());
		
	}
	
	
	

	
	
	
	/**
	 * Identification-RequestInput-AskSq3-ValSq2-positiv.json
	 * Erwartet: ElicitSlot für seucrityQuestionTwo
	 * @throws IOException
	 */
	@Test
	public void IdentificationRequestInputAskSq3ValSq2Positiv() throws IOException {

		InputStream input = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("src/test/resources/requests/Identification-RequestInput-AskSq3-ValSq2-positiv.json")));
		OutputStream output = new ByteArrayOutputStream();
		
		handler.handleRequest(input, output, ctx);
		
		//In JSON Objekt wandeln.
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonOutput = output.toString();
		JsonNode rootNode = objectMapper.readTree(jsonOutput);
		
		assertEquals("securityQuestionThree", rootNode.at("/dialogAction/slotToElicit").asText());
		assertEquals("PlainText", rootNode.at("/dialogAction/message/contentType").asText());
		assertNotNull(rootNode.at("/dialogAction/slots/securityQuestionTwo").asText());
		
	}
	
	
	/**
	 * Identification-RequestInput-AskSq3-ValSq2-negativ.json
	 * Problem: 	Nur Sparkonto angegeben, obwohl sie Spar und Privatkonto hat.
	 * Testinput:	Identification-RequestInput-SecurityQuestion2-negativ.json
	 * Output:		
	 * @throws IOException
	 */
	@Test
	public void IdentificationRequestInputAskSq3ValSq2Negativ() throws IOException {

		InputStream input = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("src/test/resources/requests/Identification-RequestInput-AskSq3-ValSq2-negativ.json")));
		OutputStream output = new ByteArrayOutputStream();
		
		handler.handleRequest(input, output, ctx);
		
		//In JSON Objekt wandeln.
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonOutput = output.toString();
		JsonNode rootNode = objectMapper.readTree(jsonOutput);
		
		assertEquals(1, rootNode.at("/sessionAttributes/securityQuestionAttemptTwo").asInt());
		assertEquals(String.valueOf("null"),rootNode.at("/dialogAction/slots/securityQuestionTwo").asText());
		

	}
	
	/**
	 * Identification-RequestInput-AskSq3-ValSq2-negativ2     (nur Sparkonto -> Ist eines zu wenig)
	 * Problem: 	SecurityQuestion1 ist 2x falsch beantwortet.
	 * Testinput:	Identification-RequestInput-SecurityQuestion2-negativ2.json
	 * Output:		Close Meldung.
	 * @throws IOException
	 */
	@Test
	public void IdentificationRequestInputAskSq3ValSq2Negativ2() throws IOException {

		InputStream input = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("src/test/resources/requests/Identification-RequestInput-AskSq3-ValSq2-negativ2.json")));
		OutputStream output = new ByteArrayOutputStream();
		
		handler.handleRequest(input, output, ctx);
		
		//In JSON Objekt wandeln.
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonOutput = output.toString();
		JsonNode rootNode = objectMapper.readTree(jsonOutput);
		
		assertEquals("Close", rootNode.at("/dialogAction/type").asText());
		assertEquals("Failed", rootNode.at("/dialogAction/fulfillmentState").asText());
		
	}

	
	
	/**
	 * Identification-RequestInput-SecurityQuestion-ValSq3-positiv.json
	 * @throws IOException
	 */
	@Test
	public void IdentificationRequestInputSecurityQuestionValSq3Positiv() throws IOException {

		InputStream input = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("src/test/resources/requests/Identification-RequestInput-SecurityQuestion-ValSq3-positiv.json")));
		OutputStream output = new ByteArrayOutputStream();
		
		handler.handleRequest(input, output, ctx);
		
		//In JSON Objekt wandeln.
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonOutput = output.toString();
		JsonNode rootNode = objectMapper.readTree(jsonOutput);
		
		assertEquals("ConfirmIntent", rootNode.at("/dialogAction/type").asText());
		assertEquals("Identification", rootNode.at("/dialogAction/intentName").asText());
	}
	
	
	/**
	 * Identification-RequestInput-SecurityQuestion-ValSq3-Negativ.json
	 * @throws IOException
	 */
	@Test
	public void IdentificationRequestInputSecurityQuestionValSq3Negativ() throws IOException {

		InputStream input = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("src/test/resources/requests/Identification-RequestInput-SecurityQuestion-ValSq3-Negativ.json")));
		OutputStream output = new ByteArrayOutputStream();
		
		handler.handleRequest(input, output, ctx);
		
		//In JSON Objekt wandeln.
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonOutput = output.toString();
		JsonNode rootNode = objectMapper.readTree(jsonOutput);
		
		assertEquals(1, rootNode.at("/sessionAttributes/securityQuestionAttemptThree").asInt());
		assertEquals(String.valueOf("null"),rootNode.at("/dialogAction/slots/securityQuestionThree").asText());
	}
	
	/**
	 * Identification-RequestInput-SecurityQuestion-ValSq3-Negativ2.json
	 * @throws IOException
	 */
	@Test
	public void IdentificationRequestInputSecurityQuestionValSq3Negativ2() throws IOException {

		InputStream input = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("src/test/resources/requests/Identification-RequestInput-SecurityQuestion-ValSq3-Negativ2.json")));
		OutputStream output = new ByteArrayOutputStream();
		
		handler.handleRequest(input, output, ctx);
		
		//In JSON Objekt wandeln.
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonOutput = output.toString();
		JsonNode rootNode = objectMapper.readTree(jsonOutput);
		
		assertEquals("Close", rootNode.at("/dialogAction/type").asText());
		assertEquals("Failed", rootNode.at("/dialogAction/fulfillmentState").asText());
		
	}
	
	
	/**
	 * Identification-RequestInput-SaveIdentification-Confirmed.json
	 * @throws IOException
	 */
	@Test
	public void IdentificationRequestInputSaveIdentificationConfirmed() throws IOException {

		InputStream input = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("src/test/resources/requests/Identification-RequestInput-SaveIdentification-Confirmed.json")));
		OutputStream output = new ByteArrayOutputStream();
		
		handler.handleRequest(input, output, ctx);
		
		//In JSON Objekt wandeln.
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonOutput = output.toString();
		JsonNode rootNode = objectMapper.readTree(jsonOutput);
		
		assertEquals("ElicitIntent", rootNode.at("/dialogAction/type").asText());
		assertEquals("Failed", rootNode.at("/dialogAction/fulfillmentState").asText());
		
	}
	
	
	/**
	 * PaymentReceived-RequestInput.json
	 * @throws IOException
	 */
	@Test
	public void PaymentReceivedRequestInput() throws IOException {

		InputStream input = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("src/test/resources/requests/PaymentReceived-RequestInput.json")));
		OutputStream output = new ByteArrayOutputStream();
		
		handler.handleRequest(input, output, ctx);
		
		//In JSON Objekt wandeln.
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonOutput = output.toString();
		JsonNode rootNode = objectMapper.readTree(jsonOutput);
		
		assertEquals("Close", rootNode.at("/dialogAction/type").asText());
		assertEquals(true, rootNode.at("/dialogAction/message/content").asText().contains("Abrasives"));
		
	}
		
	
	/**
	 * PaymentSent-RequestInput.json
	 * Input Betrag "100"
	 * @throws IOException
	 */
	@Test
	public void PaymentSentRequestInput() throws IOException {

		InputStream input = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("src/test/resources/requests/PaymentSent-RequestInput.json")));
		OutputStream output = new ByteArrayOutputStream();
		
		handler.handleRequest(input, output, ctx);
		
		//In JSON Objekt wandeln.
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonOutput = output.toString();
		JsonNode rootNode = objectMapper.readTree(jsonOutput);
		
		assertEquals("Close", rootNode.at("/dialogAction/type").asText());
		assertEquals(true, rootNode.at("/dialogAction/message/content").asText().contains("Emma"));
		
	}
	
	/**
	 * PaymentSent-RequestInput2.json
	 * Input Betrag "100Fr"
	 * @throws IOException
	 */
	@Test
	public void PaymentSentRequestInput2() throws IOException {

		InputStream input = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("src/test/resources/requests/PaymentSent-RequestInput2.json")));
		OutputStream output = new ByteArrayOutputStream();
		
		handler.handleRequest(input, output, ctx);
		
		//In JSON Objekt wandeln.
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonOutput = output.toString();
		JsonNode rootNode = objectMapper.readTree(jsonOutput);
		
		assertEquals("Close", rootNode.at("/dialogAction/type").asText());
		assertEquals(true, rootNode.at("/dialogAction/message/content").asText().contains("Emma"));
		
	}
	
}
