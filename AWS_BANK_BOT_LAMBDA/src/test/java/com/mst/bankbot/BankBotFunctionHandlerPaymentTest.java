package com.mst.bankbot;

import static org.junit.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.FileUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class BankBotFunctionHandlerPaymentTest {

	private static TestContext ctx;
	private static BankBotFunctionHandler handler;

    @BeforeClass
    public static void createInput() throws IOException {
    	ctx = new TestContext();
    	ctx.setFunctionName("BankBotFunctionHandler");
    	handler = new BankBotFunctionHandler();
    }


    
	/**
	 * Payment-Request-Test1.json
	 * @throws IOException
	 */
	@Test
	public void PaymentRequestTest1() throws IOException {

		InputStream input = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("src/test/resources/requests/paymentreceive/Payment-Request-Test1.json")));
		OutputStream output = new ByteArrayOutputStream();

		handler.handleRequest(input, output, ctx);
		
		//In JSON Objekt wandeln.
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonOutput = output.toString();
		JsonNode rootNode = objectMapper.readTree(jsonOutput);

		assertTrue(rootNode.at("/dialogAction/message/content").asText().contains("28.03.2019"));
	}
	
	/**
	 * Payment-Request-Test2.json
	 * @throws IOException
	 */
	@Test
	public void PaymentRequestTest2() throws IOException {

		InputStream input = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("src/test/resources/requests/paymentreceive/Payment-Request-Test2.json")));
		OutputStream output = new ByteArrayOutputStream();

		handler.handleRequest(input, output, ctx);
		
		//In JSON Objekt wandeln.
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonOutput = output.toString();
		JsonNode rootNode = objectMapper.readTree(jsonOutput);

		assertTrue(rootNode.at("/dialogAction/message/content").asText().contains("07.02.2019"));
	}
	
	
	/**
	 * Payment-Request-Test2.json
	 * @throws IOException
	 */
	@Test
	public void PaymentRequestTest3() throws IOException {

		InputStream input = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("src/test/resources/requests/paymentreceive/Payment-Request-Test3.json")));
		OutputStream output = new ByteArrayOutputStream();

		handler.handleRequest(input, output, ctx);
		
		//In JSON Objekt wandeln.
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonOutput = output.toString();
		JsonNode rootNode = objectMapper.readTree(jsonOutput);

		assertTrue(rootNode.at("/dialogAction/message/content").asText().contains("19.01.2019"));
	}
    
	
	/**
	 * Payment-Request-Test2.json
	 * Amount Test für 3000Fr
	 * @throws IOException
	 */
	@Test
	public void PaymentRequestTest4() throws IOException {

		InputStream input = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("src/test/resources/requests/paymentreceive/Payment-Request-Test4.json")));
		OutputStream output = new ByteArrayOutputStream();

		handler.handleRequest(input, output, ctx);
		
		//In JSON Objekt wandeln.
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonOutput = output.toString();
		JsonNode rootNode = objectMapper.readTree(jsonOutput);

		assertTrue(rootNode.at("/dialogAction/message/content").asText().contains("19.01.2019"));
	}
	
	/**
	 * Payment-Request-Test2.json
	 * Amount Test für 3000Fr.
	 * @throws IOException
	 */
	@Test
	public void PaymentRequestTest5() throws IOException {

		InputStream input = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("src/test/resources/requests/paymentreceive/Payment-Request-Test5.json")));
		OutputStream output = new ByteArrayOutputStream();

		handler.handleRequest(input, output, ctx);
		
		//In JSON Objekt wandeln.
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonOutput = output.toString();
		JsonNode rootNode = objectMapper.readTree(jsonOutput);

		assertTrue(rootNode.at("/dialogAction/message/content").asText().contains("19.01.2019"));
	}
	
}
