package com.mst.bankbot.db;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

import com.mst.bankbot.db.model.AnswerToQuestionItem;
import com.mst.bankbot.db.model.CorrectAnswerItem;
import com.mst.bankbot.db.model.FalseAnswerItem;
import com.mst.bankbot.db.model.PersonItem;

public class CreateUserTest {

	private static Object input;
	private static DynamoDBService db;

	@BeforeClass
	public static void createInput() throws IOException {
		// TODO: set up your sample input object here.
		input = null;
		db = DynamoDBService.getInstance();
	}

	@Test
    public void createUserP1() {
    	

    	PersonItem p1 = new PersonItem("1963775793750586","Margrit","Fehr");
    	p1.setIdentifed("false");
    	//q1  Auf wenn lautet dieses Konto?
    	p1.addAnswerToQuestionList(setupP1Question2(2));  	//Wer ist ihr persönlicher Kundenberater?
    	p1.addAnswerToQuestionList(setupP1Question3(3));		//Wann haben Sie die Kundenbeziehung mit uns eröffnet?
    	p1.addAnswerToQuestionList(setupP1Question4(4));		//Was für Konten führen Sie bei uns?
    	p1.addAnswerToQuestionList(setupP1Question5(5));		//Wie viele Konten führen Sie bei uns?
    	p1.addAnswerToQuestionList(setupP1Question6(6));		//Wie hoch ist der aktuelle Kontostand vom genannten Konto?
    	p1.addAnswerToQuestionList(setupP1Question7(7));		//An wen gingen die letzten Zahlungen die sie getätigt haben?
    	p1.addAnswerToQuestionList(setupP1Question8(8));		//Wie hoch war Ihr letzter Bargeldbezug?
    	p1.addAnswerToQuestionList(setupP1Question9(9));		//Wie lautet die Kartennummer der ***-Karte?
    	p1.addAnswerToQuestionList(setupP1Question10(10));		//Wie hoch ist Ihre Monatslimite der ***-Karte?
    	
    	//Payments
    	p1.addPaymentReceiveItem("Baumer Electric AG",100,"28.03.2019");
    	p1.addPaymentReceiveItem("Sia Abrasives Industries AG",100,"07.02.2019");
    	p1.addPaymentReceiveItem("Chemtura Corporation",1000,"10.03.2019");
    	p1.addPaymentReceiveItem("Zuckerfabrik Aarberg",250,"05.01.2019");
    	p1.addPaymentReceiveItem("Absorbin",3000,"27.01.2019");
    	p1.addPaymentReceiveItem("General Dynamics European Land Systems",150,"07.02.2019");
    	p1.addPaymentReceiveItem("Chocolat Bernrain AG",320,"02.04.2019");
    	p1.addPaymentReceiveItem("Amcor Flexibles Kreuzlingen AG",160,"08.01.2019");
    	p1.addPaymentReceiveItem("Alcan Packaging",170,"23.01.2019");
    	p1.addPaymentReceiveItem("Ifolor",1500,"17.02.2019");
    	p1.addPaymentReceiveItem("Privatkliniken Seeschau, Herz- und Neuro-Zentrum Bodensee, Klinik Bellevuepark",3000,"19.01.2019");
    	p1.addPaymentReceiveItem("Eugster/Frismag AG",2000,"12.03.2019");
    	p1.addPaymentReceiveItem("Max Zeller Söhne AG",1675,"18.03.2019");
    	p1.addPaymentReceiveItem("Eftec AG",850,"23.02.2019");
    	p1.addPaymentReceiveItem("proxomed Medizintechnik",970,"02.04.2019");
    	p1.addPaymentReceiveItem("Bernina Nähmaschinenfabrik, Gegauf Fritz AG",520,"19.01.2019");
    	p1.addPaymentReceiveItem("Alpina Versicherung",620,"20.03.2019");
    	
    	p1.addPaymentSentItem("Emma Müller",500,"07.01.2019");
    	p1.addPaymentSentItem("Mia Schmid",250,"10.02.2019");
    	p1.addPaymentSentItem("Sofia Keller",570,"19.03.2019");
    	p1.addPaymentSentItem("Lina Huber",840,"12.04.2019");
    	p1.addPaymentSentItem("Laura Becker",80,"18.03.2019");
    	p1.addPaymentSentItem("Elena Stadler",2100,"03.02.2019");
    	p1.addPaymentSentItem("Sara Bühler",300,"18.03.2019");
    	p1.addPaymentSentItem("Anna Thurnherr",340,"02.01.2019");
    	p1.addPaymentSentItem("Elin Schneider",1250,"05.03.2019");
    	p1.addPaymentSentItem("Noah Toppius",760,"07.03.2019");
    	p1.addPaymentSentItem("Liam Kappeler",810,"28.02.2019");
    	p1.addPaymentSentItem("Leon König",750,"13.03.2019");
    	p1.addPaymentSentItem("David Strähl",172,"19.03.2019");
    	p1.addPaymentSentItem("Nico Keller",285,"03.04.2019");
    	p1.addPaymentSentItem("Nino Graf",950,"12.04.2019");
    	p1.addPaymentSentItem("Leo Hausamann",760,"28.03.2019");
    	p1.addPaymentSentItem("Julian Klingler",430,"25.03.2019");
    	p1.addPaymentSentItem("Levin Baumann",415,"20.03.2019");
    	p1.addPaymentSentItem("Samuel Kaufmann",620,"19.01.2019");	
    	
    	PersonItem p2 = new PersonItem("xxxxxx","Lukas","Inhausen");
    	p1.setIdentifed("false");
    	//q1  Auf wenn lautet dieses Konto?
    	p2.addAnswerToQuestionList(setupP2Question2(2));  	//Wer ist ihr persönlicher Kundenberater?
    	p2.addAnswerToQuestionList(setupP2Question3(3));		//Wann haben Sie die Kundenbeziehung mit uns eröffnet?
    	p2.addAnswerToQuestionList(setupP2Question4(4));		//Was für Konten führen Sie bei uns?
    	p2.addAnswerToQuestionList(setupP2Question5(5));		//Wie viele Konten führen Sie bei uns?
    	p2.addAnswerToQuestionList(setupP2Question6(6));		//Wie hoch ist der aktuelle Kontostand vom genannten Konto?
    	p2.addAnswerToQuestionList(setupP2Question7(7));		//An wen gingen die letzten Zahlungen die sie getätigt haben?
    	p2.addAnswerToQuestionList(setupP2Question8(8));		//Wie hoch war Ihr letzter Bargeldbezug?
    	p2.addAnswerToQuestionList(setupP2Question9(9));		//Wie lautet die Kartennummer der ***-Karte?
    	p2.addAnswerToQuestionList(setupP2Question10(10));		//Wie hoch ist Ihre Monatslimite der ***-Karte?
    	
    	p2.addPaymentReceiveItem("Baumer Electric AG",100,"28.03.2019");
    	p2.addPaymentReceiveItem("Sia Abrasives Industries AG",100,"07.02.2019");
    	p2.addPaymentReceiveItem("Chemtura Corporation",1000,"10.03.2019");
    	p2.addPaymentReceiveItem("Zuckerfabrik Aarberg",250,"05.01.2019");
    	p2.addPaymentReceiveItem("Absorbin",3000,"27.01.2019");
    	p2.addPaymentReceiveItem("General Dynamics European Land Systems",150,"07.02.2019");
    	p2.addPaymentReceiveItem("Chocolat Bernrain AG",320,"02.04.2019");
    	p2.addPaymentReceiveItem("Amcor Flexibles Kreuzlingen AG",160,"08.01.2019");
    	p2.addPaymentReceiveItem("Alcan Packaging",170,"23.01.2019");
    	p2.addPaymentReceiveItem("Ifolor",1500,"17.02.2019");
    	p2.addPaymentReceiveItem("Privatkliniken Seeschau, Herz- und Neuro-Zentrum Bodensee, Klinik Bellevuepark",3000,"19.01.2019");
    	p2.addPaymentReceiveItem("Eugster/Frismag AG",2000,"12.03.2019");
    	p2.addPaymentReceiveItem("Max Zeller Söhne AG",1675,"18.03.2019");
    	p2.addPaymentReceiveItem("Eftec AG",850,"23.02.2019");
    	p2.addPaymentReceiveItem("proxomed Medizintechnik",970,"02.04.2019");
    	p2.addPaymentReceiveItem("Bernina Nähmaschinenfabrik, Gegauf Fritz AG",520,"19.01.2019");
    	p2.addPaymentReceiveItem("Alpina Versicherung",620,"20.03.2019");
    	
    	p2.addPaymentSentItem("Emma Müller",500,"07.01.2019");
    	p2.addPaymentSentItem("Mia Schmid",250,"10.02.2019");
    	p2.addPaymentSentItem("Sofia Keller",570,"19.03.2019");
    	p2.addPaymentSentItem("Lina Huber",840,"12.04.2019");
    	p2.addPaymentSentItem("Laura Becker",80,"18.03.2019");
    	p2.addPaymentSentItem("Elena Stadler",2100,"03.02.2019");
    	p2.addPaymentSentItem("Sara Bühler",300,"18.03.2019");
    	p2.addPaymentSentItem("Anna Thurnherr",340,"02.01.2019");
    	p2.addPaymentSentItem("Elin Schneider",1250,"05.03.2019");
    	p2.addPaymentSentItem("Noah Toppius",760,"07.03.2019");
    	p2.addPaymentSentItem("Liam Kappeler",810,"28.02.2019");
    	p2.addPaymentSentItem("Leon König",750,"13.03.2019");
    	p2.addPaymentSentItem("David Strähl",172,"19.03.2019");
    	p2.addPaymentSentItem("Nico Keller",285,"03.04.2019");
    	p2.addPaymentSentItem("Nino Graf",950,"12.04.2019");
    	p2.addPaymentSentItem("Leo Hausamann",760,"28.03.2019");
    	p2.addPaymentSentItem("Julian Klingler",430,"25.03.2019");
    	p2.addPaymentSentItem("Levin Baumann",415,"20.03.2019");
    	p2.addPaymentSentItem("Samuel Kaufmann",620,"19.01.2019");	
    	
    	db.createUser(p1);
    	db.createUser(p2);
    }

	private AnswerToQuestionItem setupP1Question2(int questionNumber) {
		CorrectAnswerItem correctAnswerItem1 = new CorrectAnswerItem("Michael Kuhn");
		correctAnswerItem1.addPossibleAnswer("Michael Kuhn");
		correctAnswerItem1.addPossibleAnswer("Michael");
		correctAnswerItem1.addPossibleAnswer("Kuhn");

		AnswerToQuestionItem answerToQuestion = new AnswerToQuestionItem(questionNumber);
		answerToQuestion.addCorrectAnswer(correctAnswerItem1);

		return answerToQuestion;
	}

	private AnswerToQuestionItem setupP2Question2(int questionNumber) {
		CorrectAnswerItem correctAnswerItem1 = new CorrectAnswerItem("Daniel Gross");
		correctAnswerItem1.addPossibleAnswer("Daniel Gross");
		correctAnswerItem1.addPossibleAnswer("Daniel");
		correctAnswerItem1.addPossibleAnswer("Gross");

		AnswerToQuestionItem answerToQuestion = new AnswerToQuestionItem(questionNumber);
		answerToQuestion.addCorrectAnswer(correctAnswerItem1);

		return answerToQuestion;
	}

	private AnswerToQuestionItem setupP1Question3(int questionNumber) {
		CorrectAnswerItem correctAnswerItem1 = new CorrectAnswerItem("24.11.2001");
		correctAnswerItem1.addPossibleAnswer("24.11.2001");
		correctAnswerItem1.addPossibleAnswer("November");
		correctAnswerItem1.addPossibleAnswer("November 2001");
		correctAnswerItem1.addPossibleAnswer("2001");
		correctAnswerItem1.addPossibleAnswer("01");

		AnswerToQuestionItem answerToQuestion = new AnswerToQuestionItem(questionNumber);
		answerToQuestion.addCorrectAnswer(correctAnswerItem1);

		return answerToQuestion;
	}

	private AnswerToQuestionItem setupP2Question3(int questionNumber) {
		CorrectAnswerItem correctAnswerItem1 = new CorrectAnswerItem("03.10.2015");
		correctAnswerItem1.addPossibleAnswer("03.10.2015");
		correctAnswerItem1.addPossibleAnswer("Oktober");
		correctAnswerItem1.addPossibleAnswer("Oktober 2015");
		correctAnswerItem1.addPossibleAnswer("2015");
		correctAnswerItem1.addPossibleAnswer("15");

		AnswerToQuestionItem answerToQuestion = new AnswerToQuestionItem(questionNumber);
		answerToQuestion.addCorrectAnswer(correctAnswerItem1);

		return answerToQuestion;
	}

	private AnswerToQuestionItem setupP1Question4(int questionNumber) {
		CorrectAnswerItem correctAnswerItem1 = new CorrectAnswerItem("Sparkonto");
		correctAnswerItem1.addPossibleAnswer("Sparkonto");
		correctAnswerItem1.addPossibleAnswer("Spar");
		correctAnswerItem1.addPossibleAnswer("Sparen3");

		CorrectAnswerItem correctAnswerItem2 = new CorrectAnswerItem("Privatkonto");
		correctAnswerItem2.addPossibleAnswer("Privatkonto");
		correctAnswerItem2.addPossibleAnswer("Privat");

		FalseAnswerItem falseAnwAnswerItem1 = new FalseAnswerItem("Alle anderen Konten");
		falseAnwAnswerItem1.addPossibleFalseAnswer("carlo");
		falseAnwAnswerItem1.addPossibleFalseAnswer("euro");
		falseAnwAnswerItem1.addPossibleFalseAnswer("fremd");
		falseAnwAnswerItem1.addPossibleFalseAnswer("anlage");
		falseAnwAnswerItem1.addPossibleFalseAnswer("geschenk");
		falseAnwAnswerItem1.addPossibleFalseAnswer("mieter");
		falseAnwAnswerItem1.addPossibleFalseAnswer("ziel");
		falseAnwAnswerItem1.addPossibleFalseAnswer("freizügigkeit");
		falseAnwAnswerItem1.addPossibleFalseAnswer("swisscanto");
		falseAnwAnswerItem1.addPossibleFalseAnswer("pensfree");

		AnswerToQuestionItem answerToQuestion4 = new AnswerToQuestionItem(questionNumber);
		answerToQuestion4.addCorrectAnswer(correctAnswerItem1);
		answerToQuestion4.addCorrectAnswer(correctAnswerItem2);
		answerToQuestion4.addFalseAnswer(falseAnwAnswerItem1);

		return answerToQuestion4;
	}

	private AnswerToQuestionItem setupP2Question4(int questionNumber) {
		CorrectAnswerItem correctAnswerItem1 = new CorrectAnswerItem("Sparkonto");
		correctAnswerItem1.addPossibleAnswer("Sparkonto");
		correctAnswerItem1.addPossibleAnswer("Spar");
		correctAnswerItem1.addPossibleAnswer("Sparen3");

		CorrectAnswerItem correctAnswerItem2 = new CorrectAnswerItem("Sparen3");
		correctAnswerItem2.addPossibleAnswer("Sparen3");
		correctAnswerItem2.addPossibleAnswer("Sparen 3");
		correctAnswerItem2.addPossibleAnswer("Sparen drei");

		CorrectAnswerItem correctAnswerItem3 = new CorrectAnswerItem("Carlo");
		correctAnswerItem2.addPossibleAnswer("Carlo");

		FalseAnswerItem falseAnwAnswerItem1 = new FalseAnswerItem("Alle anderen Konten");
		falseAnwAnswerItem1.addPossibleFalseAnswer("euro");
		falseAnwAnswerItem1.addPossibleFalseAnswer("fremd");
		falseAnwAnswerItem1.addPossibleFalseAnswer("anlage");
		falseAnwAnswerItem1.addPossibleFalseAnswer("geschenk");
		falseAnwAnswerItem1.addPossibleFalseAnswer("mieter");
		falseAnwAnswerItem1.addPossibleFalseAnswer("ziel");
		falseAnwAnswerItem1.addPossibleFalseAnswer("freizügigkeit");
		falseAnwAnswerItem1.addPossibleFalseAnswer("swisscanto");
		falseAnwAnswerItem1.addPossibleFalseAnswer("pensfree");

		AnswerToQuestionItem answerToQuestion4 = new AnswerToQuestionItem(questionNumber);
		answerToQuestion4.addCorrectAnswer(correctAnswerItem1);
		answerToQuestion4.addCorrectAnswer(correctAnswerItem2);
		answerToQuestion4.addCorrectAnswer(correctAnswerItem3);
		answerToQuestion4.addFalseAnswer(falseAnwAnswerItem1);

		return answerToQuestion4;
	}

	private AnswerToQuestionItem setupP1Question5(int questionNumber) {
		CorrectAnswerItem correctAnswerItem1 = new CorrectAnswerItem("2");
		correctAnswerItem1.addPossibleAnswer("2");
		correctAnswerItem1.addPossibleAnswer("zwei");

		AnswerToQuestionItem answerToQuestion = new AnswerToQuestionItem(questionNumber);
		answerToQuestion.addCorrectAnswer(correctAnswerItem1);

		return answerToQuestion;
	}

	private AnswerToQuestionItem setupP2Question5(int questionNumber) {
		CorrectAnswerItem correctAnswerItem1 = new CorrectAnswerItem("3");
		correctAnswerItem1.addPossibleAnswer("3");
		correctAnswerItem1.addPossibleAnswer("drei");

		AnswerToQuestionItem answerToQuestion = new AnswerToQuestionItem(questionNumber);
		answerToQuestion.addCorrectAnswer(correctAnswerItem1);

		return answerToQuestion;
	}

	private AnswerToQuestionItem setupP1Question6(int questionNumber) {
		CorrectAnswerItem correctAnswerItem1 = new CorrectAnswerItem("20000");

		AnswerToQuestionItem answerToQuestion = new AnswerToQuestionItem(questionNumber);
		answerToQuestion.addCorrectAnswer(correctAnswerItem1);

		return answerToQuestion;
	}

	private AnswerToQuestionItem setupP2Question6(int questionNumber) {
		CorrectAnswerItem correctAnswerItem1 = new CorrectAnswerItem("80000");

		AnswerToQuestionItem answerToQuestion = new AnswerToQuestionItem(questionNumber);
		answerToQuestion.addCorrectAnswer(correctAnswerItem1);

		return answerToQuestion;
	}

	private AnswerToQuestionItem setupP1Question7(int questionNumber) {
		CorrectAnswerItem correctAnswerItem1 = new CorrectAnswerItem("Klaus Heckmann");
		correctAnswerItem1.addPossibleAnswer("Klaus Heckmann");
		correctAnswerItem1.addPossibleAnswer("Klaus");
		correctAnswerItem1.addPossibleAnswer("Heckmann");

		AnswerToQuestionItem answerToQuestion = new AnswerToQuestionItem(questionNumber);
		answerToQuestion.addCorrectAnswer(correctAnswerItem1);

		return answerToQuestion;
	}

	private AnswerToQuestionItem setupP2Question7(int questionNumber) {
		CorrectAnswerItem correctAnswerItem1 = new CorrectAnswerItem("Claudia Ulmann");
		correctAnswerItem1.addPossibleAnswer("Claudia Ulmann");
		correctAnswerItem1.addPossibleAnswer("Claudia");
		correctAnswerItem1.addPossibleAnswer("Ulmann");

		AnswerToQuestionItem answerToQuestion = new AnswerToQuestionItem(questionNumber);
		answerToQuestion.addCorrectAnswer(correctAnswerItem1);

		return answerToQuestion;
	}

	private AnswerToQuestionItem setupP1Question8(int questionNumber) {
		CorrectAnswerItem correctAnswerItem1 = new CorrectAnswerItem("100");
		correctAnswerItem1.addPossibleAnswer("100");
		correctAnswerItem1.addPossibleAnswer("100Fr.");

		AnswerToQuestionItem answerToQuestion = new AnswerToQuestionItem(questionNumber);
		answerToQuestion.addCorrectAnswer(correctAnswerItem1);

		return answerToQuestion;
	}

	private AnswerToQuestionItem setupP2Question8(int questionNumber) {
		CorrectAnswerItem correctAnswerItem1 = new CorrectAnswerItem("200");
		correctAnswerItem1.addPossibleAnswer("200");
		correctAnswerItem1.addPossibleAnswer("200Fr.");

		AnswerToQuestionItem answerToQuestion = new AnswerToQuestionItem(questionNumber);
		answerToQuestion.addCorrectAnswer(correctAnswerItem1);

		return answerToQuestion;
	}

	private AnswerToQuestionItem setupP1Question9(int questionNumber) {
		CorrectAnswerItem correctAnswerItem1 = new CorrectAnswerItem("5502 3574 0150 8102");
		correctAnswerItem1.addPossibleAnswer("5502 3574 0150 8102");
		correctAnswerItem1.addPossibleAnswer("5502357401508102");

		AnswerToQuestionItem answerToQuestion = new AnswerToQuestionItem(questionNumber);
		answerToQuestion.addCorrectAnswer(correctAnswerItem1);

		return answerToQuestion;
	}

	private AnswerToQuestionItem setupP2Question9(int questionNumber) {
		CorrectAnswerItem correctAnswerItem1 = new CorrectAnswerItem("6702 4818 0567 8103");
		correctAnswerItem1.addPossibleAnswer("6702 4818 0567 8103");
		correctAnswerItem1.addPossibleAnswer("6702481805678103");

		AnswerToQuestionItem answerToQuestion = new AnswerToQuestionItem(questionNumber);
		answerToQuestion.addCorrectAnswer(correctAnswerItem1);

		return answerToQuestion;
	}

	private AnswerToQuestionItem setupP1Question10(int questionNumber) {
		CorrectAnswerItem correctAnswerItem1 = new CorrectAnswerItem("5000");
		correctAnswerItem1.addPossibleAnswer("5000");

		AnswerToQuestionItem answerToQuestion = new AnswerToQuestionItem(questionNumber);
		answerToQuestion.addCorrectAnswer(correctAnswerItem1);

		return answerToQuestion;
	}

	private AnswerToQuestionItem setupP2Question10(int questionNumber) {
		CorrectAnswerItem correctAnswerItem1 = new CorrectAnswerItem("7000");
		correctAnswerItem1.addPossibleAnswer("7000");

		AnswerToQuestionItem answerToQuestion = new AnswerToQuestionItem(questionNumber);
		answerToQuestion.addCorrectAnswer(correctAnswerItem1);

		return answerToQuestion;
	}

}
