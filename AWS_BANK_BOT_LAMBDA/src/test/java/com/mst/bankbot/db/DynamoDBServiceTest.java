package com.mst.bankbot.db;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;

import org.junit.BeforeClass;
import org.junit.Test;

import com.mst.bankbot.TestContext;
import com.mst.bankbot.db.model.PaymentReceiveItem;
import com.mst.bankbot.db.model.PaymentSentItem;
import com.mst.bankbot.logging.LogService;

public class DynamoDBServiceTest {

	
    private static Object input;
    private static DynamoDBService db;

    @BeforeClass
    public static void createInput() throws IOException {
		TestContext ctx = new TestContext();
		ctx.setFunctionName("BankBotFunctionHandler");
		LogService.setContext(ctx);
        db = DynamoDBService.getInstance();
    }

    
    
    @Test
    public void getAllPaymentReceivedFromPerson() {

    	ArrayList<PaymentReceiveItem> paymentReceivedList1 = db.getAllPaymentReceivedFromPerson("1963775793750586", "100", "28.03.2019");
    	ArrayList<PaymentReceiveItem> paymentReceivedList2 = db.getAllPaymentReceivedFromPerson("1963775793750586", "100", "07.02.2019");
    	ArrayList<PaymentReceiveItem> paymentReceivedList3 = db.getAllPaymentReceivedFromPerson("1963775793750586", "150", "07.02.2019");
    	ArrayList<PaymentReceiveItem> paymentReceivedList4 = db.getAllPaymentReceivedFromPerson("1963775793750586", "160", "08.01.2019");
    	ArrayList<PaymentReceiveItem> paymentReceivedList5 = db.getAllPaymentReceivedFromPerson("1963775793750586", "170", "23.01.2019");
    	ArrayList<PaymentReceiveItem> paymentReceivedList6 = db.getAllPaymentReceivedFromPerson("1963775793750586", "250", "05.01.2019");
    	ArrayList<PaymentReceiveItem> paymentReceivedList7 = db.getAllPaymentReceivedFromPerson("1963775793750586", "320", "02.04.2019");
    	ArrayList<PaymentReceiveItem> paymentReceivedList8 = db.getAllPaymentReceivedFromPerson("1963775793750586", "520", "19.01.2019");
    	ArrayList<PaymentReceiveItem> paymentReceivedList9 = db.getAllPaymentReceivedFromPerson("1963775793750586", "620", "20.03.2019");
    	ArrayList<PaymentReceiveItem> paymentReceivedList10 = db.getAllPaymentReceivedFromPerson("1963775793750586", "850", "23.02.2019");
    	ArrayList<PaymentReceiveItem> paymentReceivedList11 = db.getAllPaymentReceivedFromPerson("1963775793750586", "970", "02.04.2019");
    	ArrayList<PaymentReceiveItem> paymentReceivedList12 = db.getAllPaymentReceivedFromPerson("1963775793750586", "1000", "10.03.2019");
    	ArrayList<PaymentReceiveItem> paymentReceivedList13 = db.getAllPaymentReceivedFromPerson("1963775793750586", "1500", "17.02.2019");
    	ArrayList<PaymentReceiveItem> paymentReceivedList14 = db.getAllPaymentReceivedFromPerson("1963775793750586", "1675", "18.03.2019");
    	ArrayList<PaymentReceiveItem> paymentReceivedList15 = db.getAllPaymentReceivedFromPerson("1963775793750586", "2000", "12.03.2019");
    	ArrayList<PaymentReceiveItem> paymentReceivedList16 = db.getAllPaymentReceivedFromPerson("1963775793750586", "3000", "27.01.2019");
    	ArrayList<PaymentReceiveItem> paymentReceivedList17 = db.getAllPaymentReceivedFromPerson("1963775793750586", "3000", "19.01.2019");

    	for(PaymentReceiveItem i : paymentReceivedList3) {
    		LogService.log(i.getReceiveDate() + " " + i.getAmount() + " " + i.getFrom());
    	}
    	
    	assertEquals(paymentReceivedList1.size(), 1);
    	assertEquals(paymentReceivedList2.size(), 2);
    	assertEquals(paymentReceivedList3.size(), 2);
    	assertEquals(paymentReceivedList4.size(), 1);
    	assertEquals(paymentReceivedList5.size(), 1);
    	assertEquals(paymentReceivedList6.size(), 1);
    	assertEquals(paymentReceivedList7.size(), 1);
    	assertEquals(paymentReceivedList8.size(), 1);
    	assertEquals(paymentReceivedList9.size(), 1);
    	assertEquals(paymentReceivedList10.size(), 1);
    	assertEquals(paymentReceivedList11.size(), 1);
    	assertEquals(paymentReceivedList12.size(), 1);
    	assertEquals(paymentReceivedList13.size(), 1);
    	assertEquals(paymentReceivedList14.size(), 1);
    	assertEquals(paymentReceivedList15.size(), 1);
    	assertEquals(paymentReceivedList16.size(), 2);
    	assertEquals(paymentReceivedList17.size(), 2);
    	
    	
    }
    

    @Test
    public void getAllPaymentSentFromPerson() {
    	ArrayList<PaymentSentItem> paymentSentList = db.getAllPaymentSentFromPerson("1963775793750586", "500", "05.01.2019");
    	
    	
    	for(PaymentSentItem i : paymentSentList) {
    		LogService.log(i.getSentDate() + " " + i.getAmount() + " " + i.getTo());
    	}
    	
    	assertEquals(paymentSentList.size(), 1);

    }

    
    @Test
    public void isAlreadyAuthenticated() {
    	//assertTrue(db.isAlreadyAuthenticated("1963775793750586"));
    	assertFalse(db.isAlreadyAuthenticated("4561"));
    }
    
    
}
