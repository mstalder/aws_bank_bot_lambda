package com.mst.bankbot.intent;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

import com.mst.bankbot.BankBotFunctionHandler;
import com.mst.bankbot.TestContext;

public class PaymentReceiveIntentTest {

	
	private static TestContext ctx;
	private static BankBotFunctionHandler handler;

	
    @BeforeClass
    public static void createInput() throws IOException {
    	ctx = new TestContext();
    	ctx.setFunctionName("BankBotFunctionHandler");
    	handler = new BankBotFunctionHandler();
    }

	
	@Test
	public void formatDateTest() {
		PaymentReceiveIntent pay = new PaymentReceiveIntent();
		String pay1 = pay.formatDate("23.01.19");
		String pay2 = pay.formatDate("23.01.2019");
		String pay3 = pay.formatDate("1.01.19");
		String pay4 = pay.formatDate("01.1.2019");
		String pay5 = pay.formatDate("Am 01.1.2019");
		String pay6 = pay.formatDate("Ich glaube es war 01.1.2019 an dem folgenden Datum");
		
		assertEquals(pay1, "23.01.2019");
		assertEquals(pay2, "23.01.2019");
		assertEquals(pay3, "01.01.2019");
		assertEquals(pay4, "01.01.2019");
		assertEquals(pay5, "01.01.2019");
		assertEquals(pay6, "01.01.2019");

	}

	
	@Test
	public void fixAmount() {
		PaymentReceiveIntent pay = new PaymentReceiveIntent();
		String amount1 = pay.fixAmount("100");
		String amount2 = pay.fixAmount("100Fr");
		String amount3 = pay.fixAmount("100Fr.");
		String amount4 = pay.fixAmount("Es waren 100Fr.");
		String amount5 = pay.fixAmount("Es waren 100Fr. so glaube ich");
		String amount6 = pay.fixAmount("Es waren 100Fr.so glaube ich");
		String amount7 = pay.fixAmount("Es waren100Fr.so glaube ich");
		
		
		
		assertEquals(amount1, "100");
		assertEquals(amount2, "100");
		assertEquals(amount3, "100");
		assertEquals(amount4, "100");
		assertEquals(amount5, "100");
		assertEquals(amount6, "100");
		assertEquals(amount7, "100");
	}
}
