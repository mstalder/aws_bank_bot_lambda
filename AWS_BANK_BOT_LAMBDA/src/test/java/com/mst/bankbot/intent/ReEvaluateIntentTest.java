package com.mst.bankbot.intent;

import static org.junit.Assert.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.io.FileUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mst.bankbot.BankBotFunctionHandler;
import com.mst.bankbot.TestContext;

public class ReEvaluateIntentTest {

	
	
	private static TestContext ctx;
	private static BankBotFunctionHandler handler;

    @BeforeClass
    public static void createInput() throws IOException {
    	ctx = new TestContext();
    	ctx.setFunctionName("BankBotFunctionHandler");
    	handler = new BankBotFunctionHandler();
    }
	
	
	/**
	 * Payment-Request-Test2.json
	 * @throws IOException
	 */
	@Test
	public void reEvaluateTest() throws IOException {

		InputStream input = new ByteArrayInputStream(FileUtils.readFileToByteArray(new File("src/test/resources/requests/reevaluate/ReEvaluate-Request-Start.json")));
		OutputStream output = new ByteArrayOutputStream();

		handler.handleRequest(input, output, ctx);
		
		//In JSON Objekt wandeln.
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonOutput = output.toString();
		JsonNode rootNode = objectMapper.readTree(jsonOutput);

		assertTrue(rootNode.at("/dialogAction/message/content").asText().contains("07.02.2019"));
	}
}
