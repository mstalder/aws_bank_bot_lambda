package com.mst.bankbot.question;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import com.mst.bankbot.TestContext;
import com.mst.bankbot.logging.LogService;
import com.mst.bankbot.question.model.QuestionItem;

public class QuestionServiceTest {

	@BeforeClass
	public static void createInput() throws IOException {
		TestContext ctx = new TestContext();
		ctx.setFunctionName("BankBotFunctionHandler");
		LogService.setContext(ctx);

	}

	@Test
	public void getAllQuestionsTest() throws IOException {

		QuestionService questionService = QuestionService.getInstance();
		List<QuestionItem> questionItemLis = questionService.getAllQuestions();
		// QuestionItem questionItemList= questionService.getQuestionRandom();

		assertEquals(questionItemLis.size() > 9, true);
	}

	@Test
	public void getQuestionRandomTest() throws IOException {

		QuestionService questionService = QuestionService.getInstance();
		QuestionItem questionItem = questionService.getQuestionRandom();

		LogService.log("Question ID: " + questionItem.getId());
		assertEquals(questionItem.getId() > 0, true);
	}

	@Test
	public void wuerfelTest() throws IOException {
		QuestionService questionService = QuestionService.getInstance();

		int zahl = questionService.wuerfel(2);
		LogService.log("Zahl: " + zahl);

		assertEquals(zahl > 2, false);
	}

	@Test
	public void getQuestionWithFilterTest() {
		QuestionService questionService = QuestionService.getInstance();
		
		ArrayList<Integer> questionNotToAskAnymoreList = new ArrayList<Integer>();
		questionNotToAskAnymoreList.add(1);
		questionNotToAskAnymoreList.add(5);
		questionNotToAskAnymoreList.add(8);
		questionNotToAskAnymoreList.add(10);
		questionNotToAskAnymoreList.add(2);

		QuestionItem qItem = questionService.getQuestionWithFilter(questionNotToAskAnymoreList, "mittel");
		
		assertNotEquals(qItem.getId(), 1);
		assertNotEquals(qItem.getId(), 2);
		assertNotEquals(qItem.getId(), 5);
		assertNotEquals(qItem.getId(), 8);
		assertNotEquals(qItem.getId(), 10);
		
		
		System.out.println(qItem.getId() + ": " + qItem.getQuestion());

		assertEquals(true, true);
	}
}
